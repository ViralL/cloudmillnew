// setup file
import { configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

configure({ adapter: new Adapter() });

export function MagicClass(props, functions, state) {
	this.props = props;
	this.state = state;
	Object.keys(functions).forEach((key) => {
		this[key] = functions[key];
	});
}
