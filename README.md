# endville.ru

## Quick Start

```bash
# Install dependencies
npm install

# Build for production
npm run build

#  Start development server
npm start

# Start testing
npm test
```

## Dependencies

- React 16+
- Redux 4+
- Eslint 5+
- Jest 24+
- Other dependencies are listed in `package.json` file
- node.js  8.12.0+


## Structure

- `actions/` - Redux actions
- `components/` - Every reusable component for the app
- `reducers/` - Redux reducers
- `container/` - Pages of the app
- `utils/` - Function of the app
