const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const commonPaths = require('./paths');

module.exports = {
	entry: [
		commonPaths.entryPath
	],
	module: {
		rules: [
			{
				enforce: 'pre',
				test: /\.(js|jsx)$/,
				loader: 'eslint-loader',
				exclude: /(node_modules)/,
				options: {
					emitWarning: process.env.NODE_ENV !== 'production'
				}
			},
			{
				test: /\.(js|jsx)$/,
				include: commonPaths.clientPath,
				exclude: /(node_modules)/,
				use: {
					loader: "babel-loader",
					options: {
						cacheDirectory: true,
						plugins: [
							"react-hot-loader/babel"
						]
					}
				}
			},
			{
				test: /\.(png|jpg|gif|svg)$/,
				use: [
					{
						loader: 'file-loader',
						options: {
							outputPath: commonPaths.imagesFolder
						}
					}
				]
			},
			{
				test: /\.(woff2|ttf|woff|eot)$/,
				use: [
					{
						loader: 'file-loader',
						options: {
							outputPath: commonPaths.fontsFolder
						}
					}
				]
			}
		]
	},
	resolve: {
		modules: ['src', '/', 'node_modules'],
		extensions: ['*', '.js', '.jsx', '.css', '.scss']
	},
	plugins: [
		new webpack.ProgressPlugin(),
		new HtmlWebpackPlugin({
			template: commonPaths.templatePath
		})
	],
};
