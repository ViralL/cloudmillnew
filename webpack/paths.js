const path = require('path');

module.exports = {
	root: path.resolve(__dirname, '../'),
	outputPath: path.resolve(__dirname, '../', 'build'),
	entryPath: path.resolve(__dirname, '../', 'src/index.js'),
	templatePath: path.resolve(__dirname, '../', 'src/index.tpl.html'),
	clientPath: path.resolve(__dirname, '../', 'src'),
	imagesFolder: 'images',
	fontsFolder: 'fonts',
	cssFolder: 'styles',
	jsFolder: 'js',
	vendorFolder: 'vendor',
	assetsFolder: 'assets'
};
