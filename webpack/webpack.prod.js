const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const commonPaths = require('./paths');
const globImporter = require('node-sass-glob-importer');
const ImageminPlugin = require('imagemin-webpack-plugin').default;
const path = require('path');
const PrerenderSPAPlugin = require('prerender-spa-plugin');
const PuppeteerRenderer = PrerenderSPAPlugin.PuppeteerRenderer;

module.exports = {
    mode: 'production',
    output: {
        filename: `${commonPaths.assetsFolder}/[name].js`,
        path: commonPaths.outputPath,
        chunkFilename: '[name].js'
    },
    module: {
        rules: [
            {
                test: /\.(css|scss)$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    {
                        loader: 'css-loader',
                        options: {
                            sourceMap: true,
                            modules: true,
                            camelCase: true,
                            localIdentName: '[local]'
                        },
                    },
                    {
                        loader: 'sass-loader',
                        options: {
                            importer: globImporter()
                        }
                    }
                ]
            }
        ]
    },
    plugins: [
        new CleanWebpackPlugin(
            {
                root: commonPaths.root
            }
        ),
        new MiniCssExtractPlugin({
            filename: `${commonPaths.assetsFolder}/[name].css`,
            chunkFilename: '[id].css'
        }),
        new CopyWebpackPlugin([
            { from: `${commonPaths.clientPath}/${commonPaths.assetsFolder}/${commonPaths.imagesFolder}`, to: `${commonPaths.assetsFolder}/${commonPaths.imagesFolder}` },
            { from: `${commonPaths.clientPath}/${commonPaths.assetsFolder}/${commonPaths.fontsFolder}`, to: `${commonPaths.assetsFolder}/${commonPaths.fontsFolder}` },
            { from: `${commonPaths.clientPath}/${commonPaths.vendorFolder}`, to: `${commonPaths.vendorFolder}` }
        ], {}),
        new PrerenderSPAPlugin({
            // Required - The path to the webpack-outputted app to prerender.
            staticDir: commonPaths.outputPath,
            routes: [ '/', '/about', '/projects', '/approach', '/contacts', '/services' ],
            renderer: new PuppeteerRenderer()
        })
    ],
    devtool: 'source-map'
};
