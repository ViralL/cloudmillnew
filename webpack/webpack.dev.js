const webpack = require('webpack');
const globImporter = require('node-sass-glob-importer');
const CopyWebpackPlugin = require('copy-webpack-plugin');

const commonPaths = require('./paths');

module.exports = {
	mode: 'development',
	output: {
		filename: '[name].js',
		path: commonPaths.outputPath,
		chunkFilename: '[name].js',
		publicPath: '/'
	},
	plugins: [
		new webpack.NamedModulesPlugin(),
		new webpack.optimize.OccurrenceOrderPlugin(),
		new webpack.HotModuleReplacementPlugin(),
		new webpack.NoEmitOnErrorsPlugin(),
		new CopyWebpackPlugin([
			{ from: `${commonPaths.clientPath}/${commonPaths.assetsFolder}/${commonPaths.imagesFolder}`, to: `${commonPaths.assetsFolder}/${commonPaths.imagesFolder}` },
			{ from: `${commonPaths.clientPath}/${commonPaths.assetsFolder}/${commonPaths.fontsFolder}`, to: `${commonPaths.assetsFolder}/${commonPaths.fontsFolder}` },
			{ from: `${commonPaths.clientPath}/${commonPaths.vendorFolder}`, to: `${commonPaths.vendorFolder}` }
		], {})
	],
	module: {
		rules: [
			{
				test: /\.(css|scss)$/,
				use: [
					'style-loader',
					{
						loader: 'css-loader',
						options: {
							sourceMap: true,
							modules: true,
							camelCase: true,
							localIdentName: '[local]'
						}
					},
					{
						loader: 'sass-loader',
						options: {
							importer: globImporter()
						}
					}
				]
			}
		]
	},
	watch: true,
	watchOptions: {
		ignored: /node_modules/
	},
	devServer: {
		compress: true,
		hot: true,
		host: 'localhost',
		port: 3006,
		open: true,
		inline: true,
		stats: {
			colors: true,
			hash: false,
			version: true,
			timings: true,
			assets: true,
			chunks: false,
			modules: false,
			reasons: false,
			children: false,
			source: true,
			errors: true,
			errorDetails: true,
			warnings: true,
			publicPath: true
		},
		publicPath: '/',
		historyApiFallback: true,
		proxy: [
			{
				"context": "/ajax/**",
				"target": "https://cloudmill.ru",
				"changeOrigin": true
			}
		]
	}
};
