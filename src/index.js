import 'babel-polyfill';
import React, {Fragment} from 'react';
import {AppContainer} from 'react-hot-loader';
import {Route, Switch} from 'react-router-dom';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';

// redux
import {Provider} from 'react-redux';
import {ConnectedRouter} from 'connected-react-router';

// Constants
import {URLS} from './Constants';

// Layouts
import App from './App';

// Store
import configureStore, {history} from './configureStore';

// containers
import Main from 'containers/Main';
import Contacts from 'containers/Contacts';
import Case from 'containers/Case';
import Services from 'containers/Services';
import About from 'containers/About';
import Approach from 'containers/Approach';
import Page404 from 'containers/Page404';
import CardDescription from 'containers/CardDescription';

import {TransitionGroup, CSSTransition} from 'react-transition-group';

const store = configureStore();


const AppRoute = ({component: Component, layout: Layout, isDark: isDark, isDarkCase, animationName: animationName}) => (
	<Route
		render={props => {
			const pathname = props.location.pathname;
			const setDarkTemplate =
				pathname === `${URLS.case}/rubl`
				|| pathname === `${URLS.case}/ladoga`
				|| pathname === `${URLS.case}/eifman`
				|| pathname === `${URLS.case}/peoplelove`;

			let caseClassName;
			switch (pathname) {
				case `${URLS.case}/rubl`:
				case `${URLS.case}/eifman`:
				case `${URLS.case}/peoplelove`:
					caseClassName = 'rubl-case';
					break;
				case `${URLS.case}/ladoga`:
					caseClassName = 'ladoga-case';
					break;
				case `${URLS.case}/mmz`:
					caseClassName = 'mmz-case';
					break;
				default:
					caseClassName = '';
			}
			return (
				<Fragment>
					<Layout
						isDark={isDarkCase ? setDarkTemplate : isDark}
						setBlackCursor={pathname === `${URLS.case}/mmz`}
						caseClassName={caseClassName}
						animationName={animationName}
					>
						<Component {...props} />
					</Layout>
				</Fragment>
			);
		}
		}
	/>
);


const render = () => {
	ReactDOM.render(
		<AppContainer>
			<Provider store={store}>
				<ConnectedRouter history={history}>
					<Route render={({location}) => (
						<TransitionGroup className="transition-group">
							<CSSTransition
								key={location.pathname}
								timeout={{enter: 1500, exit: 1500}}
								classNames={'page'}
							>
								<Switch key={location.key} location={location}>
									<AppRoute
										exact
										isDark={false}
										path={URLS.case}
										layout={App}
										component={Case}
										animationName={'approachAnimation'}
									/>
									<AppRoute
										exact
										isDarkCase
										isDark={false}
										path={`${URLS.case}/test`}
										layout={App}
										component={CardDescription}
										animationName={'approachAnimation'}
									/>
									<AppRoute
										exact
										isDarkCase
										isDark={false}
										path={`${URLS.case}/ladoga`}
										layout={App}
										component={CardDescription}
										animationName={'approachAnimation'}
									/>
									<AppRoute
										exact
										isDarkCase
										isDark={false}
										path={`${URLS.case}/sanfor`}
										layout={App}
										component={CardDescription}
										animationName={'approachAnimation'}
									/>
									<AppRoute
										exact
										isDarkCase
										isDark={false}
										path={`${URLS.case}/peoplelove`}
										layout={App}
										component={CardDescription}
										animationName={'approachAnimation'}
									/>
									<AppRoute
										exact
										isDarkCase
										isDark={false}
										path={`${URLS.case}/eifman`}
										layout={App}
										component={CardDescription}
										animationName={'approachAnimation'}
									/>
									<AppRoute
										exact
										isDarkCase
										isDark={false}
										path={`${URLS.case}/pride`}
										layout={App}
										component={CardDescription}
										animationName={'approachAnimation'}
									/>
									<AppRoute
										exact
										isDarkCase
										isDark={false}
										path={`${URLS.case}/gmz`}
										layout={App}
										component={CardDescription}
										animationName={'approachAnimation'}
									/>
									<AppRoute
										exact
										isDarkCase
										isDark={false}
										path={`${URLS.case}/rubl`}
										layout={App}
										component={CardDescription}
										animationName={'approachAnimation'}
									/>
									<AppRoute
										exact
										isDarkCase
										isDark={false}
										path={`${URLS.case}/deka`}
										layout={App}
										component={CardDescription}
										animationName={'approachAnimation'}
									/>
									<AppRoute
										exact
										isDarkCase
										isDark={false}
										path={`${URLS.case}/mmz`}
										layout={App}
										component={CardDescription}
										animationName={'approachAnimation'}
									/>
									<AppRoute
										exact
										isDark
										path={URLS.approach}
										layout={App}
										component={Approach}
										animationName={'approachAnimation'}
									/>
									<AppRoute
										exact
										isDark
										path={URLS.about}
										layout={App}
										component={About}
										animationName={'approachAnimation'}
									/>
									<AppRoute
										exact
										path={URLS.services}
										layout={App}
										component={Services}
										animationName={'approachAnimation'}
									/>
									<AppRoute
										exact
										isDark
										path={URLS.contacts}
										layout={App}
										component={Contacts}
										animationName={'approachAnimation'}
									/>
									<AppRoute
										exact
										isDark={false}
										path={URLS.index}
										layout={App}
										component={Main}
										animationName={'approachAnimation'}
									/>
									<AppRoute
										exact
										isDark
										path={URLS.page404}
										layout={App}
										component={Page404}
										animationName={'approachAnimation'}
									/>
								</Switch>
							</CSSTransition>
						</TransitionGroup>
					)}/>
				</ConnectedRouter>
			</Provider>
		</AppContainer>,
		document.getElementById('root')
	);
};

render();

// Hot reloading
// if (module.hot) {
// // Reload components
// 	module.hot.accept('./App', () => {
// 		render();
// 	});
// }

AppRoute.propTypes = {
	animationName: PropTypes.string,
	component: PropTypes.func,
	location: PropTypes.object,
	layout: PropTypes.func,
	isDarkCase: PropTypes.bool,
	isDark: PropTypes.bool,
};
