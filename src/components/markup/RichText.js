import React, {Component} from 'react';
import PropTypes from 'prop-types';
import HtmlToReact, {Parser} from 'html-to-react';
import classNames from 'classnames';

const htmlToReactParser = new Parser();
const processNodeDefinitions = new HtmlToReact.ProcessNodeDefinitions(React);

export default class RichText extends Component {
	render() {
		const {
			tag = 'span',
			text,
			id
		} = this.props;

		let reactElement = htmlToReactParser.parseWithInstructions(text, () => true, [
			{
				shouldProcessNode: () => true,
				processNode: processNodeDefinitions.processDefaultNode
			}
		]);

		reactElement = reactElement ? reactElement : text;

		const Tag = tag;

		const className = classNames(
			this.props.className
		);

		return (
			<Tag className={className}
				id={id}
			>
				{reactElement}
			</Tag>
		);
	}
}

RichText.propTypes = {
	className: PropTypes.string,
	tag: PropTypes.string,
	text: PropTypes.string,
	id: PropTypes.string
};
