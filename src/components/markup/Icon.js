import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

const Icon = props => {
	const classes = classNames(
		props.className,
		'icon',
		`icon-${props.name}`
	);

	return props.name ?
		<span
			{...props.attributes}
			className={classes}
		/> : null;

};

Icon.propTypes = {
	name: PropTypes.string,
	className: PropTypes.string,
	attributes: PropTypes.any,
	ifClicked: PropTypes.func
};

Icon.defaultProps = {
};

export default Icon;
