import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

const List = props => {

	const containerClasses = classNames(props.className, {
		'list-group': props.isVisible,
		'list-check': props.isCheck
	});

	return (
		<ol role="list" className={containerClasses}>
			{props.items.map((item, index) => {
				return (
					<li role="listitem" key={index}>
						{item.text}
					</li>
				);
			})}
		</ol>
	);
};

List.propTypes = {
	className: PropTypes.string,
	label: PropTypes.string,
	isVisible: PropTypes.bool,
	items: PropTypes.array,
	role: PropTypes.string,
	isCheck: PropTypes.bool
};

List.defaultProps = {
	isVisible: true,
	spaced: false,
	schema: {
		numbered: false
	}
};

export default List;
