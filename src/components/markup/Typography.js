import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

const Typography = props => {

	const containerClasses = classNames(
		props.className, props.status
	);

	return React.createElement(props.tag, {
		className: containerClasses,
	}, props.text || props.children);

};

Typography.propTypes = {
	tag: PropTypes.string,
	className: PropTypes.string,
	text: PropTypes.oneOfType([
		PropTypes.number,
		PropTypes.string
	]),
	status: PropTypes.string,
	children: PropTypes.any
};

Typography.defaultProps = {};

export default Typography;
