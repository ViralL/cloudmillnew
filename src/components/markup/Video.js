import React from 'react';
import PropTypes from 'prop-types';

const Video = props => {
	return (
		<div className="videoContainer">
			<iframe
				src={props.src}
				style={{width: '100%', height: '100%'}}
				frameBorder="0"
				allow="autoplay; fullscreen"
				allowFullScreen
			/>
		</div>
	);
};

Video.propTypes = {
	src: PropTypes.string,
};

Video.defaultProps = {
};

export default Video;
