import React from 'react';
import PropTypes from 'prop-types';
import Row from '../grid/Row';
import Col from '../grid/Col';
import Typography from './Typography';
import {Link} from 'react-router-dom';

const NextCase = ({item, history}) => {
	return (
		<Link
			to={item.nextLink}
			className={`${item.className === 'mmz' ? 'frame hoverLink' : ''} nextPage`}
			style={item.nextBack && {backgroundImage: `url(${item.nextBack})`}}
			onClick={(e) => {
				e.preventDefault();
				history.push(item.nextLink);
			}}
		>
			<div className={'container'}>
				{item.nextImg ? (
					<Row className={'between-xs middle-xs'}>
						<Col lg={7} md={6} xs={8}>
							<Typography tag={'h5'} text={'Следующий проект'}/>
							<Typography tag={'h2'} text={item.nextText}/>
						</Col>
						<Col lg={5} md={6} xs={4} className={'text-right'}>
							<img src={item.nextImg} alt={item.nextText}/>
						</Col>
					</Row>
				) : (
					<Col xl={7} lg={8} md={9}>
						<Typography
                        	tag={'h5'}
                        	text={'Следующий проект'}
                        	className={`${item.className === 'mmz' ? 'frame hoverLink' : ''}`}
						/>
						<Typography
                        	tag={'h2'}
                        	text={item.nextText}
                        	className={`${item.className === 'mmz' ? 'frame hoverLink' : ''}`}
						/>
					</Col>
				)}
			</div>
		</Link>
	);
};

NextCase.propTypes = {
	history: PropTypes.object,
	item: PropTypes.object,
};

NextCase.defaultProps = {};

export default NextCase;
