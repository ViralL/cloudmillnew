import React from 'react';
import List from './List';
import Typography from './Typography';

const items = [
	{text: 'item'},
	{text: 'item'},
	{text: 'item'},
	{text: 'item'}
];


const Markup = () => {
	return (
		<div>
			<h4>Icons</h4>
			<div className="rte">
				<Typography tag={'h1'} text={'Heading 1'}/>
				<Typography tag={'h2'} text={'Heading 2'}/>
				<Typography tag={'h3'} text={'Heading 3'}/>
				<Typography tag={'h4'} text={'Heading 4'}/>
				<Typography tag={'h5'} text={'Heading 5'}/>
				<Typography tag={'h6'} text={'Heading 6'}/>
				<Typography tag={'p'}>
					<strong>Regular text</strong>: <i>Italic</i>, <u>Underlined</u> Lorem ipsum dolor sit amet,
                    consectetur adipisicing elit.
                    Recusandae, sequi? Corporis ipsam repellendus blanditiis eaque, explicabo eum nostrum temporibus
                    cum, repellat sequi minima illum nihil? Eligendi
                    eius, sapiente earum mollitia.
				</Typography>
				<Typography tag={'p'}>
					<a href="#">Link text</a>
				</Typography>
			</div>
			<List items={items} isCheck/>
		</div>
	);
};

export default Markup;
