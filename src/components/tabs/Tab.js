import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Typography from '../markup/Typography';

class Tab extends Component {

	onClick = () => {
		const { label, onClick } = this.props;
		onClick(label);
	};

	render() {
		const {
			onClick,
			props: {
				activeTab,
				label
			}
		} = this;
		let className = 'tabs-item';

		if (activeTab === label) {
			className += ' active';
		}

		return (
			<li
				className={className}
				onClick={onClick}
			>
				<Typography tag={'div'} text={label} />
			</li>
		);
	}
}

Tab.propTypes = {
	icon: PropTypes.string,
	isValid: PropTypes.array,
	activeTab: PropTypes.string.isRequired,
	label: PropTypes.any.isRequired,
	onClick: PropTypes.func.isRequired
};

export default Tab;
