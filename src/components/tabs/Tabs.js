import React, { Component } from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import PropTypes from 'prop-types';
import Fade from 'react-reveal/Fade';

import {setPoint} from 'actions/PointActions';

import {DELAY_ROUTING, DURATION, POINT} from 'Constants';

// components
import Tab from './Tab';

class Tabs extends Component {
	constructor(props) {
		super(props);
		this.state = {
			activeTab: this.props.tab.tab || this.props.children[0].props.label
		};
	}

	onClickTabItem = (tab) => {
		this.props.setPoint(POINT.reload);
		this.setState({ activeTab: tab });
	};

	render() {
		const {
			onClickTabItem,
			props: {
				children,
				point,
			},
			state: {
				activeTab
			}
		} = this;

		return (
			<div className="tabs-container">
				<Fade bottom duration={DURATION} delay={point === POINT.routing ? DELAY_ROUTING : 0}>
					<div className={'container services'}>
						<div className={'col-xl-10 col-lg-11 center-block'}>
							<ul className="tabs">
								{children.map((child) => {
									const { label } = child.props;
									return (
										<Tab
											activeTab={activeTab}
											key={label}
											label={label}
											onClick={onClickTabItem}
										/>
									);
								})}
							</ul>
						</div>
					</div>
				</Fade>
				<div className="tabs-content">
					{children.map((child) => {
						if (child.props.label !== activeTab) return undefined;
						return child.props.children;
					})}
				</div>
			</div>
		);
	}
}


Tabs.propTypes = {
	children: PropTypes.instanceOf(Array).isRequired,
	point: PropTypes.string,
	tab: PropTypes.object,
	ifCliked: PropTypes.func,
	setPoint: PropTypes.func,
};

const mapStateToProps = (state) => ({
	tab: state.tab,
});

const mapDispatchToProps = dispatch => ({
	setPoint: bindActionCreators(setPoint, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(Tabs);
