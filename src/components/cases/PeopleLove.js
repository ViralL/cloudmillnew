import React from 'react';
import PropTypes from 'prop-types';

const PeopleLove = ({item}) => {
	return (
		<>
			<div
				className="pp-page-1"
				style={{background: `url(${item.style2}) no-repeat top center / 100% auto`}}
			>
				<div className={'text-right'}><img className={'style1'} src={item.style01} alt=""/></div>
				<div className={'container-long'}>
					<img className={'img1'} src={item.images2} alt={item.title}/>
				</div>
			</div>
			<div
				className="pp-page-2"
				style={{background: `url(${item.style4}) no-repeat top 15% center / 100% auto`}}
			>
				<div className={'container-long'}>
					<div className={'text-right'}><img src={item.style3} className={'style1'} alt=""/></div>
					<img className={'img1'} src={item.images3} alt={item.title}/>
				</div>
			</div>
			<div
				className="pp-page-3"
				style={{background: `url(${item.style6}) no-repeat top 20% center / 100% auto`}}
			>
				<div className={'text-right'}><img src={item.style5} className={'style1'} alt=""/></div>
				<div className={'container-long'}>
					<img className={'img1'} src={item.images4} alt={item.title}/>
					<div className={'text-right'}><img className={'img2'} src={item.images5} alt={item.title}/>
					</div>
				</div>
			</div>
			<video className={'video'} width="100%" autoPlay={'autoplay'} loop>
				<source src={item.video2} type='video/mp4; codecs="avc1.42E01E, mp4a.40.2"'/>
			</video>
			<div className="pp-page-4">
				<div className={'text-right'}><img className={'style1'} src={item.style7} alt=""/></div>
				<img className={'img1'} src={item.images6} alt={item.title}/>
				<img className={'img2'} src={item.images7} alt={item.title}/>
				<img className={'img3'} src={item.images8} alt={item.title}/>
			</div>
			<div className="pp-page-5">
				<img className={'img1'} src={item.images9} alt={item.title}/>
				<img className={'img2'} src={item.images10} alt={item.title}/>
				<img className={'gif'} src={item.gif} alt={item.title}/>
				<div className={'container-long text-center'}>
					<img className={'img3'} src={item.images11} alt={item.title}/>
				</div>
				<div className={'text-right'}><img className={'img4'} src={item.images12} alt={item.title}/>
				</div>
				<div className={'text-center container'}><img className={'img5'} src={item.images13}
															  alt={item.title}/></div>
			</div>
		</>
	);
};

PeopleLove.propTypes = {
	item: PropTypes.object,
};

export default PeopleLove;
