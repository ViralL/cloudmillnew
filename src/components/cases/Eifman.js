import React from 'react';
import PropTypes from 'prop-types';

const Eifman = ({item}) => {
	return (
        <>
            <div className="eifman-dark">
            	<img className={''} src={item.images1} alt={item.title}/>
            	<img className={'style1'} src={item.images2} alt={item.title}/>
            	<div className={'container relative'}>
            		<iframe
            			src="https://player.vimeo.com/video/372412500"
            			style={{width: '100%'}}
            			height={800}
            			frameBorder="0"
            			allow="autoplay; fullscreen"
            			allowFullScreen
            			className={'style2'}
            		/>
            	</div>
            </div>
            <div className="eifman-medium">
            	<img className={'style3'} src={item.images3} alt={item.title}/>
            	<div className="text-right">
            		<img className={'style2'} src={item.images4} alt={item.title}/>
            	</div>
            </div>
            <div className="eifman-dark">
            	<img className={'style3'} src={item.images5} alt={item.title}/>
            	<img className={'style3'} src={item.images6} alt={item.title}/>
            </div>
            <div className="eifman-copper">
            	<div className="text-center">
            		<img className={'style3'} src={item.gif} alt={item.title}/>
            	</div>
            </div>
            <div className="eifman-medium">
            	<img className={'style3'} src={item.images7} alt={item.title}/>
            </div>
            <div className="eifman-dark">
            	<div className="text-right">
            		<img className={'style3'} src={item.images8} alt={item.title}/>
            		<img className={'style3'} src={item.images9} alt={item.title}/>
            	</div>
            </div>
            <div className="eifman-copper">
            	<img className={'style3'} src={item.images10} alt={item.title}/>
            </div>
        </>
	);
};

Eifman.propTypes = {
	item: PropTypes.object,
};

export default Eifman;
