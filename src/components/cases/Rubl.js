import React from 'react';
import PropTypes from 'prop-types';
import Col from '../grid/Col';

const Rubl = ({item}) => {
	return (
        <>
            <div className={'container relative pad3b'}>
            	{item.style10 && <img src={item.style10} alt={item.title}/>}
            	<div className={'marg1b'}/>
            	{item.style5 && <img className={'style2 hidden-xs'} src={item.style5} alt={item.title}/>}
            	{item.images3 && <img src={item.images3} className={'img-left'} alt={item.title}/>}
            	<div className={'marg4b'}/>
            </div>
            <div className={'inner-block'} style={{backgroundImage: `url(${item.style6})`}}>
            	<div className={'container relative'}>
            		{item.images4 && <img src={item.images4} className={'style1'} alt={item.title}/>}
            		<div className={'marg3b'}/>
            		{item.style11 && <img src={item.style11} alt={item.title}/>}
            		<div className={'marg1b'}/>
            		{item.style7 && <img className={'style3  hidden-xs'} src={item.style7} alt={item.title}/>}
            	</div>
            	{item.images5 &&
                <div className={'text-center'}><img src={item.images5} alt={item.title}/></div>}
            </div>
            {item.style8 && <img className={'style4'} src={item.style8} alt={item.title}/>}
            <div className={'container relative pad2t'}>
            	{item.images6 && <div className={'image'}><img src={item.images6} alt={item.title}/></div>}
            	<div className={'marg2b'}/>
            </div>
            {item.images7 && (
            	<div className={'text-center'}>
            		<img src={item.images7} className={'notfully relative'}
            			alt={item.title}/>
            	</div>
            )}
            <div className={'container relative pad2t'}>
            	<div className={'marg3b'}/>
            	{item.style12 && <img className={'style5  hidden-xs'} src={item.style12} alt={item.title}/>}
            	{item.images8 && <img src={item.images8} className={'text-right'} alt={item.title}/>}
            	<div className={'marg3b'}/>
            </div>
            <div className={'text-center'}>
            	{item.images9 && <img src={item.images9} className={'text-right'} alt={item.title}/>}
            	<div className={'marg3b'}/>
            	{item.images10 && <img src={item.images10} className={'text-right'} alt={item.title}/>}
            	{item.images11 && <img src={item.images11} className={'text-right'} alt={item.title}/>}
            	{item.images12 && <img src={item.images12} className={'text-right'} alt={item.title}/>}
            </div>
            <div className={'marg3b'}/>
            <div className={'container'}>
            	<Col lg={8} className={'margauto'}>
            		{item.images13 && <img src={item.images13} alt={item.title}/>}
            		<div className={'marg3b'}/>
            	</Col>
            </div>
            {item.images14 && <div className={'text-center'}><img src={item.images14} alt={item.title}/></div>}
            <div className={'marg3b'}/>
        </>
	);
};

Rubl.propTypes = {
	item: PropTypes.object,
};

export default Rubl;
