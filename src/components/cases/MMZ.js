import React from 'react';
import PropTypes from 'prop-types';
import Row from '../grid/Row';
import Col from '../grid/Col';

const MMZ = ({item}) => {
	return (
        <>
            <div className={'container'}>
            	<div className={'mmz-inner'}>
            		<Row className={'bottom-xs between-xs'}>
            			<Col>
            				<img src={item.style1} alt=""/>
            			</Col>
            			<Col>
            				<img src={item.style2} alt=""/>
            			</Col>
            		</Row>
            	</div>
            </div>
            <div
            	className={'mmz-one relative'}
            	style={{background: `url(${item.style3}) repeat-x top center / 120% auto`}}
            >
            	<div className={'container relative'}>
            		<img src={item.images1} alt=""/>
            		<div className={'marg2t'}/>
            		<img src={item.images2} alt=""/>
            		<div className={'marg2t'}/>
            		{/* <video width="100%" controls="controls" height="900px" poster={item.style3}>
								<source src={item.video} type='video/mp4; codecs="avc1.42E01E, mp4a.40.2"'/>
                                Тег video не поддерживается вашим браузером.
							</video> */}
            		<iframe
            			src="https://player.vimeo.com/video/354728855"
            			style={{width: '100%'}}
            			frameBorder="0"
            			allow="autoplay; fullscreen"
            			allowFullScreen
            		/>
            		<div className={'marg2t'}/>
            		<img src={item.images3} alt="" className={'indexPage--white'}/>
            		<div className={'marg3t'}/>
            	</div>
            	<img src={item.style4} alt="" className={'style1 hidden-xs'}/>
            </div>
            <div className={'mmz-white'}>
            	<div className={'container relative'}>
            		<img src={item.style5} alt="" className={'style2'}/>
            		<div className={'marg2t'}/>
            		<img src={item.images4} alt=""/>
            		<div className={'marg3t'}/>
            		<img src={item.style6} alt="" className={'style2'}/>
            		<div className={'marg2t'}/>
            		<img src={item.images5} alt=""/>
            		<div className={'marg3t'}/>
            		<img src={item.images6} alt=""/>
            		<div className={'marg2t'}/>
            		<img src={item.images7} alt="" className={'style3'}/>
            		<div className={'marg2t'}/>
            	</div>
            	<img src={item.images8} alt=""/>
            </div>
            <div className={'container mmz-st'}>
            	<img src={item.style7} alt="" className={'style2'}/>
            </div>
            <img src={item.style8} alt="" className={'style4 hidden-xs'}/>
            <div className={'container mmz-bt'}>
            	<img src={item.images9} alt=""/>
            	<div className={'marg2t'}/>
            </div>
            <div className={'mmz-bg'}><img src={item.images10} alt=""/></div>
            <div className={'mmz-white'}>
            	<div className={'container'}>
            		<img src={item.images11} alt="" className={'style6'}/>
            	</div>
            	<img src={item.style8} alt="" className={'style5 hidden-xs'}/>
            	<div className={'container text-right'}>
            		<img src={item.images12} alt="" className={'style6 style7'}/>
            		<div className={'marg2t'}/>
            	</div>
            	<div className={'container relative'}>
            		<img src={item.gif1} alt=""/>
            		<div className={'marg2t'}/>
            		<img src={item.gif2} alt=""/>
            		<div className={'marg2t'}/>
            		<img src={item.gif3} alt=""/>
            		<div className={'marg2t'}/>
            		<img src={item.style10} alt="" className={'style2'}/>
            	</div>
            	<img src={item.style11} alt="" className={'style5 hidden-xs'}/>
            	<div className={'container mmz-wt'}>
            		<img src={item.images13} alt=""/>
            	</div>
            </div>
        </>
	);
};

MMZ.propTypes = {
	item: PropTypes.object,
};

export default MMZ;
