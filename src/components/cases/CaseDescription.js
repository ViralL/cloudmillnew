import React from 'react';
import PropTypes from 'prop-types';
import Row from '../grid/Row';
import Col from '../grid/Col';
import Typography from '../markup/Typography';
import RichText from '../markup/RichText';

const CaseDescription = ({item}) => {
	return (
		<div className="card-desc">
			<div className={'container relative'}>
				<Row className={'between-xs'}>
					{item.title && <Col md={6} xs={12}>
						<Typography tag={'h1'}>{item.title}</Typography>
					</Col>}
					{item.desc && <Col md={6} xs={12}>
						<RichText tag={'p'} text={item.desc}/>
					</Col>}
				</Row>
				<Row className={'between-xs bottom-xs'}>
					{item.type && (
						<Col md={6} xs={12}>
							{item.link && (
								<a href={item.link} target={'_blank'}
									className={'button magnetize button--red'}>
                                    Посмотреть сайт
									<img src="/assets/images/btn-arr.svg" alt={item.title}/>
								</a>
							)}
							<Typography tag={'p'} text={item.type}/>
						</Col>
					)}
					{item.technology && (
						<Col md={6} xs={12}>
							<Typography tag={'p'} text={item.technology}/>
						</Col>
					)}
					{item.awards && item.awards.length && (
						<Col xs={12} className={'flex'}>
							{item.awards.map((award) => {
								return (
									<div className="flex-item">
										<img src={award.icon} alt={award.title}/>
										<Typography tag={'p'} text={award.title}/>
										<Typography tag={'small'} text={award.type}/>
									</div>
								);
							})}
						</Col>
					)}
				</Row>
			</div>
		</div>
	);
};

CaseDescription.propTypes = {
	item: PropTypes.object,
};

export default CaseDescription;
