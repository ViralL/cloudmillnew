import React from 'react';
import PropTypes from 'prop-types';

const Ladoga = ({item}) => {
	return (
		<>
			<video className={'video'} width="100%" muted control={'true'} autoPlay={'autoplay'} loop>
				<source src={item.video} type='video/mp4; codecs="avc1.42E01E, mp4a.40.2"'/>
			</video>
			<div className="ladoga-dark">
				<div className="container">
					<img className={'style2'} src={item.style01} alt={item.title}/>
					<img className={''} src={item.images1} alt={item.title}/>
				</div>
			</div>
			<div className="ladoga-medium">
				<div className="container">
					<img className={''} src={item.images02} alt={item.title}/>
				</div>
			</div>
			<div className="ladoga-dark">
				<div className="container">
					<img className={''} src={item.images3} alt={item.title}/>
				</div>
			</div>
			<div className="ladoga-medium">
				<div className="container">
					<img className={'style1'} src={item.images4} alt={item.title}/>
					<img className={''} src={item.images5} alt={item.title}/>
				</div>
			</div>
			<div className="ladoga-copper">
				<div className="container">
					<img className={'style2'} src={item.style2} alt={item.title}/>
					<img className={''} src={item.images6} alt={item.title}/>
				</div>
			</div>
			<div className="ladoga-dark">
				<div className="container">
					<div className="text-right"><img className={'style2'} src={item.style3} alt={item.title}/>
					</div>
					<div className="style3 style1"><img className={''} src={item.images7} alt={item.title}/>
					</div>
					<img className={'style1'} src={item.images8} alt={item.title}/>
					<img className={'style1'} src={item.images9} alt={item.title}/>
					<img className={''} src={item.images10} alt={item.title}/>
				</div>
			</div>
			<div className="ladoga-medium">
				<div className="container">
					<img className={''} src={item.gif} alt={item.title}/>
				</div>
			</div>
			<div className="ladoga-copper">
				<div className="container">
					<img className={''} src={item.images11} alt={item.title}/>
				</div>
			</div>
			<img className={''} src={item.images12} alt={item.title}/>
		</>
	);
};

Ladoga.propTypes = {
	item: PropTypes.object,
};

export default Ladoga;
