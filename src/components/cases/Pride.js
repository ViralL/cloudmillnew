import React from 'react';
import PropTypes from 'prop-types';
import Video from '../markup/Video';

const Pride = ({item}) => {
	return (
		<div className={'text-center'}>
			<div className="pride-medium style1">
            	<img className={'style2'} src={item.images1} alt={item.title}/>
            	<img className={''} src={item.images2} alt={item.title}/>
			</div>
			<Video src={'https://player.vimeo.com/video/393481287'}/>
			<img src={item.images3} alt={item.title}/>
			<div className="pride-light style3">
            	<img className={''} src={item.images4} alt={item.title}/>
			</div>
			<img className={'gif'} src={item.gif} alt={item.title}/>
			<img className={'gif'} src={item.gif2} alt={item.title}/>
			<div className="pride-light style4">
            	<img className={''} src={item.images5} alt={item.title}/>
			</div>
			<div className="pride-medium style1">
            	<img className={''} src={item.images6} alt={item.title}/>
			</div>
			<img src={item.style2} alt={item.title}/>
			<div className="pride-light style5">
            	<img className={''} src={item.images7} alt={item.title}/>
			</div>
			<img className={'gif'} src={item.gif3} alt={item.title}/>
			<img className={''} src={item.images8} alt={item.title}/>
			<div className="pride-light style6">
            	<img className={''} src={item.images9} alt={item.title}/>
			</div>
			<img className={''} src={item.images10} alt={item.title}/>
			<div className="pride-medium style7">
            	<img className={'style8'} src={item.style1} alt={item.title}/>
            	<img className={''} src={item.images11} alt={item.title}/>
			</div>
			<div className="pride-light style9">
            	<img className={''} src={item.images12} alt={item.title}/>
            	<img className={'style10'} src={item.images13} alt={item.title}/>
            	<img className={'style11'} src={item.images14} alt={item.title}/>
			</div>
			<div className="pride-medium style12">
            	<img className={'style13'} src={item.images15} alt={item.title}/>
            	<img className={'style14'} src={item.images16} alt={item.title}/>
            	<img className={''} src={item.images17} alt={item.title}/>
			</div>
		</div>
	);
};

Pride.propTypes = {
	item: PropTypes.object,
};

export default Pride;
