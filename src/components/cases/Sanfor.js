import React from 'react';
import PropTypes from 'prop-types';

const Sanfor = ({item}) => {
	return (
		<>
			<div className="card-line"
				 style={{background: `url(${item.style1}) no-repeat bottom 100px left -100px / 33vw auto`}}>
				<div className={'container relative'}>
					<img className={'marg3b'} src={item.images1} alt={item.title}/>
					{item.style2 && <img className={'style3 style2'} src={item.style2} alt={item.title}/>}
				</div>
				<img className={'img-comp'} src={item.images2} alt={item.title}/>
			</div>
			<div className="card-img">
				<div className={'container'}>
					<ul className={'card-img-list marg2b'}>
						<li>
							<div className={'marg3b'}/>
							{item.images3 && <img src={item.images3} alt={item.title}/>}
						</li>
						<li>
							{item.images4 && <img src={item.images4} alt={item.title}/>}
							{item.images5 && <img src={item.images5} alt={item.title}/>}
						</li>
						{item.images6 && <li><img src={item.images6} alt={item.title}/></li>}
					</ul>
					{item.images7 &&
					<div className={'whiteBack'}><img className={''} src={item.images7} alt={item.title}/>
					</div>}
				</div>
			</div>
			<div className="card-back">
				{item.style3 && <div className={'decor marg2b'}
									 style={{background: `url(${item.style3}) repeat-x center / auto 100%`}}/>}
				{item.images8 &&
				<div className={'marg2b marg2t'}><img className={''} src={item.images8} alt={item.title}/>
				</div>}
				<div className={'container'}>
					{item.images9 &&
					<div className={'marg2b marg2t'}><img className={''} src={item.images9} alt={item.title}/>
					</div>}
					{item.images10 &&
					<div className={'marg2b marg2t'}><img className={''} src={item.images10} alt={item.title}/>
					</div>}
					{item.images11 &&
					<div className={'marg2b marg2t'}><img className={''} src={item.images11} alt={item.title}/>
					</div>}
				</div>
			</div>
			<div className="card-list"
				 style={{background: `url(${item.style2}) no-repeat bottom 100px right -1% / 17vw auto, #f6f6f6`}}>
				<div className={'container'}>
					{item.images12 &&
					<div className={''}><img className={''} src={item.images12} alt={item.title}/></div>}
				</div>
				{item.images13 &&
				<div className={'marg2b marg-2t'}><img className={''} src={item.images13} alt={item.title}/>
				</div>}
				<div className={'container relative'}>
					{item.images14 &&
					<div className={''}><img className={''} src={item.images14} alt={item.title}/></div>}
					{item.style2 && <img className={'style2'} src={item.style2} alt={item.title}/>}
				</div>
			</div>
		</>
	);
};

Sanfor.propTypes = {
	item: PropTypes.object,
};

export default Sanfor;
