import React from 'react';
import PropTypes from 'prop-types';

const Deka = ({item}) => {
	return (
		<>
			<div className={'deka-white'}>
				<div className="container text-center">
					<img src={item.style2} alt={item.title} className={'style1'}/>
					<img src={item.images3} alt={item.title}/>
					<iframe
						src="https://player.vimeo.com/video/362271043"
						style={{width: '100%'}}
						frameBorder="0"
						allow="autoplay; fullscreen"
						allowFullScreen
					/>
					<img src={item.gif} alt={item.title} className={'style1'}/>
				</div>
				<img src={item.images5} alt={item.title} className={'style3'}/>
				<img src={item.images6} alt={item.title} className={'style3 style1'}/>
				<img src={item.images7} alt={item.title} className={'style4'}/>
				<img src={item.images8} alt={item.title}/>
				<img src={item.images9} alt={item.title} className={'style4'}/>
			</div>
			<div className="deka-grey">
				<img src={item.images10} alt={item.title}/>
				<img src={item.images11} alt={item.title} className={'style5'}/>
			</div>
			<div className={'deka-white'} style={{background: `url(${item.style4}) no-repeat center`}}>
				<div className="container text-center">
					<img src={item.images12} alt={item.title} className={'style5'}/>
					<img src={item.images13} alt={item.title} className={'style6'}/>
					<img src={item.images14} alt={item.title} className={'style5'}/>
				</div>
			</div>
		</>
	);
};

Deka.propTypes = {
	item: PropTypes.object,
};

export default Deka;
