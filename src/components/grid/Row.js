import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { mapToCssModules } from 'utils/Grid';

const Row = ( props ) => {
	const {
		tag: Tag,
		...attributes
	} = props;

	const classes = mapToCssModules(classNames(
		props.className,
		props.noGutters ? 'no-gutters' : null,
		'row'
	), props.cssModule);

	return (
		<Tag {...attributes} className={classes} />
	);
};

Row.propTypes = {
	tag: PropTypes.oneOfType([PropTypes.func, PropTypes.string]),
	noGutters: PropTypes.bool,
	className: PropTypes.string,
	cssModule: PropTypes.object
};

Row.defaultProps = {
	className: '',
	tag: 'div'
};

export default Row;
