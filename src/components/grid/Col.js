import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { mapToCssModules } from 'utils/Grid';

const colWidths = ['xs', 'sm', 'md', 'lg', 'xl'];
const stringOrNumberProp = PropTypes.oneOfType([PropTypes.number, PropTypes.string]);

const columnProps = PropTypes.oneOfType([
	PropTypes.bool,
	PropTypes.number,
	PropTypes.string,
	PropTypes.shape({
		size: PropTypes.oneOfType([PropTypes.bool, PropTypes.number, PropTypes.string]),
		push: stringOrNumberProp,
		pull: stringOrNumberProp,
		offset: stringOrNumberProp
	})
]);

const getColumnSizeClass = (isXs, colWidth, colSize) => {
	if (colSize === true || colSize === '') {
		return isXs ? 'col' : `col-${colWidth}`;
	} else if (colSize === 'auto') {
		return isXs ? 'col-auto' : `col-${colWidth}-auto`;
	}

	return `col-${colWidth}-${colSize}`;
};

const Col = ( props ) => {
	const {
		className,
		cssModule,
		widths,
		tag: Tag,
		...attributes
	} = props;
	const colClasses = [];

	widths.forEach((colWidth, i) => {
		let columnProp = props[colWidth];

		if (!i && columnProp === undefined) {
			columnProp = true;
		}

		delete attributes[colWidth];

		if (!columnProp && columnProp !== '') {
			return;
		}

		const isXs = !i;
		const colClass = getColumnSizeClass(isXs, colWidth, columnProp);
		colClasses.push(colClass);
	});

	const classes = mapToCssModules(classNames(
		className,
		colClasses
	), cssModule);

	return (
		<Tag {...attributes} className={classes} />
	);
};

Col.propTypes = {
	tag: PropTypes.oneOfType([PropTypes.func, PropTypes.string]),
	xs: columnProps,
	sm: columnProps,
	md: columnProps,
	lg: columnProps,
	xl: columnProps,
	className: PropTypes.string,
	cssModule: PropTypes.object,
	widths: PropTypes.array
};

Col.defaultProps = {
	tag: 'div',
	widths: colWidths
};

export default Col;
