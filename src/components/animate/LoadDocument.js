import React from 'react';
import PropTypes from 'prop-types';

const LoadDocument = ({white}) => (
	<div className={white ? 'load load--white' : 'load'}>
		<div className="lds-ring">
			<div/>
			<div/>
			<div/>
			<div/>
		</div>
	</div>
);

LoadDocument.propTypes = {
	white: PropTypes.bool
};

LoadDocument.defaultProps = {
	white: false,
};


export default LoadDocument;
