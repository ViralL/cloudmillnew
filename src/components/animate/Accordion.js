import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { Parser } from 'html-to-react';


export default class Accordion extends Component {
	state = {
		show: false,
		selected: -1
	}

	handleClick = (index) => {
		this.setState({
			selected: this.state.selected === index ? -1 : index
		});
	};

	render() {
		const htmlToReactParser = new Parser();
		return (
			<div className="accordion">
				{
					this.props.array.map((item, index) => {
						return (
							<div
								key={index}
								className={classNames('accordion-item', { 'active': this.state.selected === index })}
							>
								<div
									className={'accordion-header'}
									onClick={() => (
										this.handleClick(index)
									)}
								>
									{htmlToReactParser.parse(item.title)}
								</div>
								<div className={'accordion-body'}>
									{htmlToReactParser.parse(item.description)}
								</div>
							</div>
						);
					})
				}
			</div>
		);
	}
}

Accordion.propTypes = {
	array: PropTypes.array,
	handleClick: PropTypes.func,
	content: PropTypes.any
};

Accordion.defaultProps = {
	array: []
};
