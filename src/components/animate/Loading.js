import React from 'react';

const Loading = () => (
	<div className="loading">
		<div className="loading-text">
			<span className="loading-text-words">У</span>
			<span className="loading-text-words">Ж</span>
			<span className="loading-text-words">Е</span>
			<span className="loading-text-words" />
			<span className="loading-text-words">П</span>
			<span className="loading-text-words">О</span>
			<span className="loading-text-words">Ч</span>
			<span className="loading-text-words">Т</span>
			<span className="loading-text-words">И</span>
		</div>
	</div>
);


export default Loading;
