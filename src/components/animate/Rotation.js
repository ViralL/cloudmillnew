import React from 'react';
import PropTypes from 'prop-types';

import Rotation from 'react-rotation';

const RotationComponent = ({items, ifChanged}) => (
	<Rotation
		cycle
		onChange={ifChanged}
	>
		{items.map((item, index) => {
			return (
				<img key={index} src={item.img} alt={''}/>
			);
		})}
	</Rotation>
);

RotationComponent.propTypes = {
	items: PropTypes.array,
	ifChanged: PropTypes.func
};


export default RotationComponent;
