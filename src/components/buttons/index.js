import React from 'react';
import Button from './Button';
import SearchButton from './SearchButton';

const Buttons = () => {
	return (
		<div>
			<h4>Theme buttons</h4>
			<SearchButton loading={false} />
			<SearchButton loading />
			<Button
				text={'btn-primary'}
				colorBtn={'primary'}
				sizeBtn={'big'}
			/>
			<Button
				text={'btn-secondary'}
				colorBtn={'secondary'}
				sizeBtn={'small'}
			/>
			<Button
				text={'btn-green'}
				colorBtn={'green'}
			/>
			<Button
				text={'btn-red'}
				colorBtn={'red'}
			/>
			<Button
				text={'btn-outlined'}
				colorBtn={'outlined'}
			/>
			<Button
				text={'btn-transparent'}
				colorBtn={'transparent'}
			/>
		</div>
	);
};

export default Buttons;
