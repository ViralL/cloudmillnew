import React from 'react';
import PropTypes from 'prop-types';

// components
// import Icon from 'react-web-vector-icons';
// import Loader from 'components/animate/Loader';

const SearchButton = props => {

	return (
		<button
			className={'btn-search'}
			onClick={props.ifClicked}
			disabled={props.disabled}
		>
			{/* {props.loading ? (*/}
			{/*	<Loader />*/}
			{/* ) : (*/}
			{/*	<Icon font="Feather" size={24} name={'search'} />*/}
			{/* )}*/}
		</button>
	);
};

SearchButton.propTypes = {
	loading: PropTypes.bool,
	disabled: PropTypes.bool,
	ifClicked: PropTypes.func
};

SearchButton.defaultProps = {
	loading: false
};

export default SearchButton;
