import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

const Button = props => {
	const buttonClasses = classNames('button', props.className, {
		'disabled': props.isDisabled,
		[`button--${props.sizeBtn}`]: props.sizeBtn,
		[`button--${props.colorBtn}`]: props.colorBtn,
		['button--transparent']: props.isTransparent
	});

	return (
		<button
			type="button"
			disabled={props.isDisabled}
			className={buttonClasses}
			onClick={props.ifClicked}
		>
			{props.text || props.children}
		</button>
	);
};

Button.propTypes = {
	ifClicked: PropTypes.func,
	className: PropTypes.string,
	sizeBtn: PropTypes.string,
	colorBtn: PropTypes.string,
	text: PropTypes.any,
	children: PropTypes.any,
	isTransparent: PropTypes.bool,
	isDisabled: PropTypes.any
};

Button.defaultProps = {
	ifClicked: null,
	isDisabled: false,
	isTransparent: false,
	text: ''
};

export default Button;
