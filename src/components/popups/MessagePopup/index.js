import { connect } from 'react-redux';
// components
import MessagePopup from './MessagePopup';


const mapStateToProps = (state) => ({
	description: state.popup.description,
	title: state.popup.title,
	buttons: state.popup.buttons
});

const mapDispatchToProps = () => ({});

export default connect(mapStateToProps, mapDispatchToProps)(MessagePopup);
