import React, { Fragment } from 'react';
import PropTypes from 'prop-types';

// components
import Typography from 'components/markup/Typography';


const MessagePopup = (props) => {
	return (
		<Fragment>
			<button className="close" onClick={props.ifClicked}><img src={'/assets/images/close.png'} /></button>
			<div className="popup-body text-center h100">
				{props.title && (
					<Typography
						text={props.title}
						tag={'h4'}
					/>
				)}
				{props.description && (
					<Typography
						text={props.description}
						tag={'h5'}
					/>
				)}
			</div>
		</Fragment>
	);
};


MessagePopup.propTypes = {
	title: PropTypes.string,
	description: PropTypes.string,
	buttons: PropTypes.array,
	ifClicked: PropTypes.func,
};


export default MessagePopup;
