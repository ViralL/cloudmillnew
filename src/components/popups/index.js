// redux
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { closePopup } from 'actions/PopupActions';

// components
import Popup from './Popup';

const mapStateToProps = (state) => ({
	popupName: state.popup.popupName
});

const mapDispatchToProps = (dispatch) => ({
	closePopup: bindActionCreators(closePopup, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(Popup);
