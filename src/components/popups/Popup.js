import React, {Component} from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

// components
import MessagePopup from './MessagePopup';

import {POPUPS} from 'Constants';

class Popup extends Component {

	handleClose() {
		this.props.closePopup();
		document.body.classList.remove('fixes');
	}

	render() {
		const containerClassName = classNames('popup', this.props.className);
		const {popupName} = this.props;

		let PopupContent = null;
		switch (popupName) {
			case POPUPS.messagePopup:
				PopupContent = MessagePopup;
				break;
			default:
				break;
		}

		if (popupName) {
			document.body.classList.add('fixes');
		}
		return popupName && (
			<div className="popup-container" onClick={() => this.handleClose()}>
				<div className={containerClassName}>
					<PopupContent className={'popup-inner'} ifClicked={() => this.handleClose()}/>
				</div>
			</div>
		);
	}
}

Popup.propTypes = {
	popupName: PropTypes.string,
	className: PropTypes.string,
	closePopup: PropTypes.func
};

Popup.defaultProps = {
	popupName: ''
};

export default Popup;
