import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Link, withRouter} from 'react-router-dom';
import Fade from 'react-reveal/Fade';

// Constants
import {DURATION, POINT, URLS, DELAY_ROUTING} from 'Constants';

// components
import Typography from 'components/markup/Typography';

export class CatalogComponent extends Component {
	constructor(props) {
		super(props);

		this.ref0 = React.createRef();
		this.ref1 = React.createRef();
		this.ref2 = React.createRef();
		this.ref3 = React.createRef();
		this.ref4 = React.createRef();
		this.ref5 = React.createRef();
		this.ref6 = React.createRef();
		this.ref7 = React.createRef();
		this.ref8 = React.createRef();

		this.handleMouseMove = this.handleMouseMove.bind(this);
		this.handleMouseLeave = this.handleMouseLeave.bind(this);
	}

	handleMouseMove(event, slide) {
		if(slide) {
			const el = slide.current;
			const r = el.getBoundingClientRect();

			el.style.setProperty('--x', event.clientX - (r.left + Math.floor(r.width / 2)));
			el.style.setProperty('--y', event.clientY - (r.top + Math.floor(r.height / 2)));
		}
	}

	handleMouseLeave(slide) {
		if(slide) {
			slide.current.style.setProperty('--x', 0);
			slide.current.style.setProperty('--y', 0);
		}
	}

	stopDefAction(evt, id, url, target) {
		evt.preventDefault();
		this.props.setCurrentItem(id);
		if(!target) {
			this.props.history.push(url);
		} else {
			window.location.href = target;
		}
	}

	render() {
		const {point} = this.props;
		return (
			<div className="catalog content content--7">
				{this.props.items.map((item, index) => {
					return (
						<Fade bottom duration={DURATION} delay={point === POINT.routing && (index === 0 || index === 1) ? DELAY_ROUTING : 0} key={index}>
							<Link
								to={`${URLS.case}/${item.url}`}
								onClick={(event) => this.stopDefAction(event, item.url, `${URLS.case}/${item.url}`, item.urlBlank)}
								className={'catalog-item hoverLink'}
								onMouseMove={event => this.handleMouseMove(event, this[`ref${index}`])}
								onMouseLeave={() => this.handleMouseLeave(this[`ref${index}`])}
							>
								<div
									ref={this[`ref${index}`]}
									className={`catalog-inner hoverLink slide ${item.customStyle}`}
								>
									<div className={'catalog-left visible-xl hoverLink'}>
										<Typography className="catalog-description hoverLink" text={item.desc} tag="div"/>
										<Typography className="catalog-title hoverLink" text={item.title} tag="h2"/>
										<div
											className="catalog-link hoverLink"
										>
											{item.id === 20 || item.id === 21 ? 'Смотреть на Behance' : 'Смотреть кейс'}
										</div>
									</div>
									<div className={'catalog-right slide__image-wrapper hoverLink'}>
										{item.img && <img className="catalog-image slide__image hoverLink" src={item.img} alt={item.title}/>}
									</div>
									<div className={'catalog-back-wrapper hoverLink'}>
										{(item.back || item.colorBack) && <div
											className={'catalog-back hoverLink'}
											style={{
												'background': `url(${item.back}) no-repeat center right / cover`,
												'backgroundColor': item.colorBack
											}}
										/>}
										{item.video && (
											// <!--<div>
											// 	<video
											// 		className={'catalog-video'}
											// 		style={{objectFit: 'cover', objectPosition: 'center right', backgroundSize: 'cover', width: '100%'}}
											// 		preload="auto" playsInline autoPlay loop muted
											// 	>
											// 		<source src={item.video} type="video/mp4" />
											// 	</video>
											// </div>-->
											<div>
												<video
													className={'catalog-video'}
													style={{display: 'block', objectFit: 'cover', backgroundSize: 'cover', width: '100%'}}
													preload="auto"
													playsInline autoPlay loop muted
												>
													<source src={item.video} type="video/mp4" />
												</video>
											</div>
										)}
									</div>
								</div>
								<div className={'catalog-left hidden-xl'}>
									<Typography className="catalog-description" text={item.desc} tag="div"/>
									<Typography className="catalog-title" text={item.title} tag="h2"/>
									<div className="catalog-link hoverLink">
                                        Смотреть кейс
									</div>
								</div>
							</Link>
						</Fade>
					);
				})}
			</div>
		);
	}
}

CatalogComponent.propTypes = {
	items: PropTypes.array,
	history: PropTypes.object,
	filter: PropTypes.string,
	point: PropTypes.string,
	ifClicked: PropTypes.func,
	setCurrentItem: PropTypes.func,
	handleChange: PropTypes.func
};

CatalogComponent.defaultProps = {
	filter: '*'
};

export default withRouter(CatalogComponent);
