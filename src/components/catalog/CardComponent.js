import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import {Redirect, withRouter} from 'react-router-dom';
import {URLS} from 'Constants';

// components
import Pride from '../cases/Pride';
import Eifman from '../cases/Eifman';
import Deka from '../cases/Deka';
import Ladoga from '../cases/Ladoga';
import Rubl from '../cases/Rubl';
import MMZ from '../cases/MMZ';
import PeopleLove from '../cases/PeopleLove';
import Sanfor from '../cases/Sanfor';
import NextCase from '../markup/NextCase';
import CaseDescription from '../cases/CaseDescription';

const CardComponent = (props) => {
	const item = props.item ? props.item.case : {};
	const keys = Object.keys(item).length;

	useEffect(() => {
		if(item.className === 'gmz') {
			window.location.href = 'http://cases.cloudmill.ru/gmz/';

			document.location = 'http://cases.cloudmill.ru/gmz/';

			if ((navigator.userAgent.match(/iPhone/i)) || (navigator.userAgent.match(/iPod/i))) {
				location.replace('http://cases.cloudmill.ru/gmz/');
			}
		}
	}, [item]);

	return keys !== 0 && item.className !== 'gmz' ? (
		<div className={classNames('card', item.className)}>
			{item.breadcrumbs && <div className={'breadcrumbs'}><img src={item.breadcrumbs} alt={item.title}/></div>}
			<div
				className="card-main vh-section-full"
				style={item.mainBackground && {background: `url(${item.mainBackground}) no-repeat center / cover ${item.mainBackgroundColor || ''}`}}
			>
				{item.className !== 'ladoga' && (
					<div className={'container text-center'}>
						{item.yearBlock && (
							<div className={'card-yearBlock'}>
								<img src={'/assets/images/cases/rubl/year.png'} alt="Cloudmill"/>
							</div>
						)}
						{item.mainImg && <img className={'mainImg'} src={item.mainImg} alt={item.title}/>}
						{item.className === 'peoplelove' && <img src={item.images1} alt={item.title}/>}
					</div>
				)}
				{item.className === 'deka' && (
					<div className={'deka-main'}>
						<img src={item.images1} alt={item.title}/>
					</div>
				)}
				{item.mainImg && item.className === 'ladoga' &&
                <img className={'mainImg'} src={item.mainImg} alt={item.title}/>}
			</div>
			{item.className !== 'mmz' && item.className !== 'eifman' && (
				<CaseDescription item={item}/>
			)}
			{item.className === 'sanfor' && <Sanfor item={item}/>}
			{item.className === 'rubl' && <Rubl item={item}/>}
			{item.className === 'mmz' && <MMZ item={item}/>}
			{item.className === 'peoplelove' && <PeopleLove item={item}/>}
			{item.className === 'deka' && <Deka item={item}/>}
			{item.className === 'ladoga' && <Ladoga item={item}/>}
			{item.className === 'eifman' && <Eifman item={item}/>}
			{item.className === 'pride' && <Pride item={item}/>}
			{item.nextLink && <NextCase item={item} history={props.history}/>}
		</div>
	) : (
		<Redirect to={URLS.case}/>
	);
};

CardComponent.propTypes = {
	item: PropTypes.object,
	history: PropTypes.object,
};

export default withRouter(CardComponent);
