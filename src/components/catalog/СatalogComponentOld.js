import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { URLS } from 'Constants';

// components
import Masonry from 'react-masonry-component';
import Typography from 'components/markup/Typography';


const CatalogComponent = props => {
	const masonryOptions = {
		transitionDuration: '0.8s',
		columnWidth: '.grid-sizer',
		gutter: '.gutter-sizer',
		itemSelector: '.grid-item',
		percentPosition: true
	};
	return (
		<Masonry
			className="catalog content content--7"
			options={masonryOptions}
		>
			<div className={'grid-sizer'}/>
			<div className={'gutter-sizer'}/>
			{props.items.map((item, index) => {
				return (props.filter === '*' || props.filter === item.type) && (
					Object.keys(item.case).length !== 0 ? (
						<Link
							key={index}
							to={`${URLS.case}/${item.id}`}
							onClick={()=>props.setCurrentId(item.id - 1)}
							className="tilter tilter--7 catalog-item grid-item"
						>
							<figure className="tilter__figure">
								<img className="tilter__image" src={item.img} alt={item.title} />
								<div className="tilter__deco tilter__deco--shine"><div /></div>
							</figure>
							<Typography className="catalog-title" text={item.title} tag="h5" />
							<Typography className="catalog-description" text={item.desc} tag="div" />
						</Link>
					) : (
						<div className="tilter tilter--7 catalog-item grid-item catalog-item-empty" key={index}>
							<figure className="tilter__figure">
								<div className={'catalog-prev'} />
								<img className="tilter__image" src={item.img} alt={item.title} />
								<div className="tilter__deco tilter__deco--shine"><div /></div>
							</figure>
							<Typography className="catalog-title" text={item.title} tag="h5" />
							<Typography className="catalog-description" text={item.desc} tag="div" />
						</div>
					)
				);
			})}
		</Masonry>
	);
};

CatalogComponent.propTypes = {
	items: PropTypes.array,
	filter: PropTypes.string,
	ifClicked: PropTypes.func,
	setCurrentId: PropTypes.func,
	handleChange: PropTypes.func
};

CatalogComponent.defaultProps = {
	filter: '*'
};

export default CatalogComponent;
