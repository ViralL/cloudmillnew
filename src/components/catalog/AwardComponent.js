import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import RichText from 'components/markup/RichText';


const AwardComponent = props => {
	return (
		<div className="awards-list">
			{props.items.map((item, index) => {
				const containerClass = classNames('awards-list_item', props.className, {
					'borders': item.img
				});
				return (
					<div className={containerClass} key={index}>
						<div className={'content-ratio'}>
							<div className={'awards-list_header'}>
								<div className={'awards-list_num'}>
									{item.place}
									<span>место</span>
								</div>
								{item.img && (
									<div className={'awards-list_icon'}>
										<img src={item.img} alt=""/>
									</div>
								)}
							</div>
							<RichText tag={'div'} className={'awards-list_desc'} text={item.desc} />
						</div>
					</div>
				);
			})}
			<div className={'awards-list_item awards-list_item-list'}>
				<div>
					<p className={'awards-list_item-title'}>Где</p>
					<p>Рейтинг рунета</p>
					<p>Tagline Awards</p>
					<p>Золотой Сайт</p>
					<p>AWWWARDS</p>
					<p>CSS Design Awards</p>
					<p>Behance</p>
				</div>
			</div>
			<div className={'awards-list_item awards-list_item-list'}>
				<div>
					<p className={'awards-list_item-title'}>Награда</p>
					<p>Золото, 2Х Серебро</p>
					<p>5X Золото, 4Х Серебро, 5Х Бронза</p>
					<p>Золото, 3Х Серебро, Бронза</p>
					<p>3X Honorable Mention</p>
					<p>4X Special Kudos</p>
					<p>XD, Interraction,
						<br /> Graphic Design, Illustrator</p>
				</div>
			</div>
			<div className={'awards-list_item borders'}>
				<div className={'content-ratio'}>
					<div className={'awards-list_header'}>
						<div className={'awards-list_title'}>
							АКАР
							<br/>
							ТОП-100
						</div>
					</div>
					<RichText tag={'div'} className={'awards-list_desc'} text={'Рейтинг digital-агентств'} />
				</div>
			</div>
		</div>
	);
};

AwardComponent.propTypes = {
	className: PropTypes.string,
	items: PropTypes.array,
	ifClicked: PropTypes.func,
	handleChange: PropTypes.func
};

export default AwardComponent;
