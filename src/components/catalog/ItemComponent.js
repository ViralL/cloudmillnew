import React from 'react';
import PropTypes from 'prop-types';
import {Redirect} from 'react-router-dom';
import {URLS} from 'Constants';

// components
import Typography from 'components/markup/Typography';
import RichText from 'components/markup/RichText';

const ItemComponent = ({item}) => {
	return item ? (
		<div className="onenews">
			<div className="onenews-item">
				<Typography tag={'h1'} className={'onenews-title'} text={item.title}/>
				<img src={item.img} alt={item.title}/>
				<Typography tag={'p'} className={'onenews-date'} text={item.date}/>
				<RichText className="onenews-description" text={item.desc} tag="div"/>
			</div>
		</div>
	) : (
		<Redirect to={URLS.news}/>
	);
}
;

ItemComponent.propTypes = {
	item: PropTypes.object,
	ifClicked: PropTypes.func,
	handleChange: PropTypes.func
};

export default ItemComponent;
