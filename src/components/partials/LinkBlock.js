import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Link, withRouter} from 'react-router-dom';

// components
import Typography from 'components/markup/Typography';

export class LinkBlock extends Component {
	constructor(props) {
		super(props);
		this.state = {
			timer: 0,
			clientWidth: document.body.clientWidth
		};
		this.timerId = 0;
	}
	componentDidMount() {
		window.addEventListener('resize', this.setScreenOrientation.bind(this));
		window.addEventListener('orientationchange', this.setScreenOrientation.bind(this));
	}

	setScreenOrientation = () => {
		setTimeout(() => {
			this.setState({clientWidth: document.body.clientWidth});
		}, 100);
	}

	goToLink = (el) => {
		this.timerId = setInterval(() => {
			this.setState({
				timer: this.state.timer + 1
			});
			if(this.state.timer === 5) {
				this.props.history.push(el.props.link);
			}
		}, 1000);

		setTimeout(() => clearInterval(this.timerId), 6000);
	}
	rejectLink = () => {
		clearInterval(this.timerId);
		this.setState({
			timer: 0
		});
	}

	render() {
		const {clientWidth} = this.state;
		const mainClass = 'frame indexPage-aboutLink hoverLink';

		return (
			<Link
				to={this.props.link}
				className={mainClass}
				onClick={(e) => {
					e.preventDefault();
					this.props.history.push(this.props.link);
				}}
				onMouseOver={e => {
					clientWidth > 767 && this.goToLink(this, e);
				}}
				onMouseLeave={() => this.rejectLink()}
			>
				<div className={'container hoverLink'}>
					<div className={'relative hoverLink'}>
						<Typography tag={'p'} text={'Следующий раздел'} className={'hoverLink'} />
						<Typography tag={'h2'} className={'hoverLink'} text={this.props.text}/>
					</div>
					<span className={'hoverLink'} />
				</div>
			</Link>
		);
	}
}

LinkBlock.propTypes = {
	link: PropTypes.string,
	onClick: PropTypes.func,
	text: PropTypes.string,
	history: PropTypes.object,
	class: PropTypes.string
};

export default withRouter(LinkBlock);
