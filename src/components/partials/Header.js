import React, {Component} from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import Fade from 'react-reveal/Fade';

import {DURATION, POINT, DELAY_ROUTING} from 'Constants';
import {Link, NavLink, withRouter} from 'react-router-dom';
import {menu} from 'mocks/menu';

// components
import Row from '../grid/Row';
import Col from '../grid/Col';
import Typography from '../markup/Typography';


class Header extends Component {
	constructor(props) {
		super(props);
		const {isDark} = this.props;
		const logoImg = (isDark) ? '/assets/images/logo/cm_logo_2_w.svg' : '/assets/images/logo/cm_logo_2.svg';
		this.state = {
			logo: logoImg,
			showMenu: false,
			count: 0
		};
	}

	showMenu() {
		this.setState({
			showMenu: !this.state.showMenu
		});
		// this.state.showMenu ? document.body.classList.remove('fixed') : document.body.classList.add('fixed');
	}

	closeMenu() {
		this.setState({
			showMenu: false
		});
		document.body.classList.remove('fixed');
		setTimeout(() => {
			window.scrollTo(0, 0);
		}, 1000);
	}


	setLogo() {
		const {isDark, color} = this.props;
		const logoImg2 = (isDark || !color) ? '/assets/images/logo/cm_logo_2_w.svg' : '/assets/images/logo/cm_logo_2.svg';
		const logoImg3 = (isDark || !color) ? '/assets/images/logo/cm_logo_3_w.svg' : '/assets/images/logo/cm_logo_3.svg';
		const images = [logoImg2, logoImg3];
		const length = images.length;

		this.setState({
			count: this.state.count + 1,
			logo: images[this.state.count]
		});

		if (this.state.count >= length - 1) {
			this.setState({count: 0});
		}
	}


	render() {
		const {className, isDark, color, point, location} = this.props;
		const {showMenu} = this.state;
		const linkClasses = classNames('header-link magnetize', className, {'active': !color});
		const mainClass = classNames('header', {'active': showMenu, 'dark': !color});
		const menuTitle = 'меню';
		const logoImg1 = (isDark || !color) ? '/assets/images/logo/cm_logo_1_w.svg' : '/assets/images/logo/cm_logo_1.svg';

		return (
			<header className={showMenu ? 'showMenu menu' : 'menu'}>
				<div className={mainClass}>
					<Fade top duration={DURATION} delay={point === POINT.routing ? DELAY_ROUTING : 0}>
						<Row className="middle-xs between-xs">
							<Link
								to={'/'}
								className={(showMenu || isDark) ? 'flip-card dark' : 'flip-card'}
								onClick={e => {
									showMenu && this.closeMenu();
									if (location.pathname === '/') {
										e.preventDefault();
									} else {
										e.preventDefault();
										this.props.history.push('/');
									}
								}}
							>
								<div className="magnetize">
									<div className="flip-card-inner" onMouseEnter={() => this.setLogo()}>
										<div className="flip-card-front"><img src={logoImg1} alt="Cloudmill"/></div>
										<div className="flip-card-back"><img src={this.state.logo} alt="Cloudmill"/>
										</div>
									</div>
								</div>
							</Link>
							<Col>
								<a className={linkClasses} onClick={() => this.showMenu()}>{menuTitle}</a>
							</Col>
						</Row>
					</Fade>
				</div>
				<div className={'navigation'}>
					<div className={'navigation-background'}>
						<div className={'navigation-content'}>
							<div className={'header'}>
								<Row className="middle-xs between-xs">
									<Link
										to="/"
										className={'flip-card dark'}
										onClick={e => {
											showMenu && this.closeMenu();
											if (location.pathname === '/') e.preventDefault();
										}}
									>
										<div className="magnetize">
											<div className="flip-card-inner" onMouseEnter={() => this.setLogo()}>
												<div className="flip-card-front">
													<img src={'/assets/images/logo/cm_logo_1_w.svg'} alt="Cloudmill"/>
												</div>
												<div className="flip-card-back">
													<img src={'/assets/images/logo/cm_logo_2_w.svg'} alt="Cloudmill"/>
												</div>
											</div>
										</div>
									</Link>
									<Col>
										<a className={'header-link magnetize active revert'}
											onClick={() => this.showMenu()}>вернуться</a>
									</Col>
								</Row>
							</div>
							<div className={'container'}>
								<Row className={'between-xs navigation-main'}>
									<Col xl={3} lg={4} xs={12}>
										<ul className={'menu'}>
											{menu.map((item, index) => {
												const itemClasses = classNames('menu-item');
												return (
													<li className={itemClasses} key={index}>
														<NavLink
															to={item.link}
															activeClassName="active"
															className="menu-link"
															onClick={() => this.closeMenu()}
														>
															{item.text}
														</NavLink>
													</li>
												);
											})}
										</ul>
									</Col>
									<Col xl={3} lg={3} sm={4} xs={6} className={'flex between-xs'}>
										<div>
											<Typography tag={'b'}>
												<a href="tel:88124256717">8 812 425 67 17</a>
											</Typography>
											<br/>
											<Typography tag={'b'}>
												<a href="mailto:info@cloudmill.ru">info@cloudmill.ru</a>
											</Typography>
										</div>
										<div className={'flex-top flex hidden-xs hidden-xs'}>
											<Typography tag={'b'} className={'linkto'}>
												<a href="http://www.cloudmill.ru/iacm.pdf"
													target="_blank">Презентация</a>
											</Typography>
											<Typography tag={'b'} className={'text-sm'}>
                                                Поможет вам быстро ознакомиться <br/>
                                                с портфолио и начать работу с нами
											</Typography>
										</div>
									</Col>
									<Col xl={3} lg={4} sm={6} xs={6} className={'flex between-xs'}>
										<Typography tag={'p'} className={'no-wrap-md'}>
                                            191015, Россия, Санкт-Петербург, <br/>
                                            Фуражный переулок д.3 литер К, оф. 317
										</Typography>
										<div className={'flex-top flex hidden-xs hidden-xs'}>
											<Typography tag={'b'} className={'linkto'}>
												<a href="https://docs.google.com/forms/d/1QUiF8vW4EVNgnRdb77thBDUVoQpTXNgS0cbMBwDmZB8/viewform?edit_requested=true"
													target="_blank">Заполнить бриф</a>
											</Typography>
											<Typography tag={'b'} className={'text-sm'}>
                                                Мы сможем предложить лучшее решение, <br/>
                                                с точной оценкой стоимости и сроков
											</Typography>
										</div>
									</Col>
									<Col md={1} sm={2} xs={12} className={'hidden-xs hidden-xs'}>
										<ul className={'navigation-soc'}>
											<li><a href="https://www.facebook.com/cloudmill/"
												target="_blank">Facebook</a>
											</li>
											<li><a href="https://www.instagram.com/cloudmill.ru/"
												target="_blank">Instagram</a></li>
											<li><a href="https://www.behance.net/cloudmill/"
												target="_blank">Behance</a>
											</li>
										</ul>
									</Col>
								</Row>
								<Row className={'navigation-footer between-xs bottom-xs'}>
									<Col xl={3} sm={4} xs={6} className={'navigation-copy'}><span>©</span> 2021
                                        CloudMill</Col>
									<Col sm={1} xs={3} className={'navigation-logo'}>
										<a href="https://www.agima.ru/" target={'_blank'}>
											<img src="/assets/images/agima.svg" alt=""/>
										</a>
									</Col>
								</Row>
							</div>
						</div>
					</div>
				</div>
			</header>
		);
	}
}

Header.propTypes = {
	className: PropTypes.string,
	location: PropTypes.object,
	history: PropTypes.object,
	point: PropTypes.string,
	isDark: PropTypes.bool,
	color: PropTypes.bool
};

export default withRouter(Header);
