import React from 'react';

// components
import moment from 'moment/moment';
import Typography from '../markup/Typography';

const Footer = () => (
	<footer className="footer">
		<div className="container">
			<Typography className={'text-secondary'} tag={'b'} text={`Copyright © ${moment().year()} Все права защищены.`} />
			<Typography tag={'div'} text={'Копирование информации размещенной на сайте, без письменного разрешения администрации строго запрещено. Скачать лаунчер онлайн можно нажав на кнопку начать игру.'} />
		</div>
	</footer>
);

export default Footer;
