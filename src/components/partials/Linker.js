import React from 'react';
import PropTypes from 'prop-types';
import Fade from 'react-reveal/Fade';
import {DURATION, POINT} from 'Constants';

const Linker = (props) => (
	<Fade left duration={DURATION} delay={props.point === POINT.routing ? 1000 : 0}>
		<div className="linker">
			<div className="magnetize">
				<div className={props.setDark ? 'dark' : 'light'}>
					<a href="https://www.behance.net/cloudmill/" target="_blank">Be</a>
					<a href="https://www.instagram.com/cloudmill.ru/" target="_blank">Inst</a>
					<a href="https://www.facebook.com/cloudmill/" target="_blank">Fb</a>
				</div>
			</div>
		</div>
	</Fade>
);

Linker.propTypes = {
	point: PropTypes.string,
	setDark: PropTypes.bool,
};

export default Linker;
