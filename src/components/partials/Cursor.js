import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

class Cursor extends Component {
	render() {
		const { mouseY, mouseX, className, clicked, setBlackCursor, isHover } = this.props;
		const mainClass = classNames('cursor', className, { 'dark': setBlackCursor });
		// const svgFill = color ? '#000' : '#fff';
		const clickStyle = { transform: 'scale(0.8)', opacity: 0.3};

		return (
			<div className={mainClass}>
				<div className="cursor-circle" style={{ transform: `translate3d(${mouseX}px, ${mouseY}px, 0px)` }} />
				<svg viewBox="0 0 30 30" className="cursor-svg" style={{ transform: `translate3d(${mouseX}px, ${mouseY}px, 0px)`}}>
					<path id="round" fill={setBlackCursor ? '#000' : '#ff6f61'} d="M15,12a3,3,0,1,1-3,3A3,3,0,0,1,15,12Z" />
				</svg>
				<div className="cursor-click" style={{ transform: `translate3d(${mouseX}px, ${mouseY}px, 0px)`}}>
					<div className="inner" style={clicked ? clickStyle : {}} />
				</div>
				{isHover && <div className="onHover" style={{ transform: `translate3d(${mouseX - 30}px, ${mouseY + 30}px, 0px)`}}>Click</div>}
			</div>
		);
	}
}

Cursor.propTypes = {
	className: PropTypes.string,
	color: PropTypes.bool,
	isHover: PropTypes.bool,
	clicked: PropTypes.bool,
	setBlackCursor: PropTypes.bool,
	mouseY: PropTypes.number,
	mouseX: PropTypes.number
};

Cursor.defaultProps = {
	color: false,
	mouseX: 0,
	mouseY: 0
};

export default Cursor;
