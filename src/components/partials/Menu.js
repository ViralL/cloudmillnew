import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import {NavLink} from 'react-router-dom';

// components
import Row from 'components/grid/Row';
import Col from 'components/grid/Col';
import Typography from 'components/markup/Typography';

const Menu = props => {
	const mainClasses = classNames('navigation', props.className);
	return (
		<div className={mainClasses}>
			<div className={'container'}>
				<Row className={'between-xs navigation-main'}>
					<Col xl={3} lg={4} xs={12}>
						<ul className={'menu'}>
							{props.items.map((item, index) => {
								const itemClasses = classNames('menu-item');
								return (
									<li className={itemClasses} key={index}>
										<NavLink
											to={item.link}
											activeClassName="active"
											className="menu-link"
											onClick={props.ifClicked}
										>
											{item.text}
										</NavLink>
									</li>
								);
							})}
						</ul>
					</Col>
					<Col xl={3} lg={3} sm={4} xs={6} className={'flex between-xs'}>
						<div>
							<Typography tag={'b'}>
								<a href="tel:88124256717">8 812 425 67 17</a>
							</Typography>
							<br/>
							<Typography tag={'b'}>
								<a href="mailto:info@cloudmill.ru">info@cloudmill.ru</a>
							</Typography>
						</div>
						<div className={'flex-top flex hidden-xs hidden-xs'}>
							<Typography tag={'b'} className={'linkto'}>
								<a href="http://www.cloudmill.ru/iacm.pdf" target="_blank">Презентация</a>
							</Typography>
							<Typography tag={'b'} className={'text-sm'}>
                                Поможет вам быстро ознакомиться <br/>
                                с портфолио и начать работу с нами
							</Typography>
						</div>
					</Col>
					<Col xl={3} lg={4} sm={6} xs={6} className={'flex between-xs'}>
						<Typography tag={'p'} className={'no-wrap-md'}>
                            191015, Россия, Санкт-Петербург, <br/>
                            Фуражный переулок д.3 литер К, оф. 317
						</Typography>
						<div className={'flex-top flex hidden-xs hidden-xs'}>
							<Typography tag={'b'} className={'linkto'}>
								<a href="https://docs.google.com/forms/d/1QUiF8vW4EVNgnRdb77thBDUVoQpTXNgS0cbMBwDmZB8/viewform?edit_requested=true" target="_blank">Заполнить бриф</a>
							</Typography>
							<Typography tag={'b'} className={'text-sm'}>
                                Мы сможем предложить лучшее решение, <br/>
                                с точной оценкой стоимости и сроков
							</Typography>
						</div>
					</Col>
					<Col md={1} sm={2} xs={12} className={'hidden-xs hidden-xs'}>
						<ul className={'navigation-soc'}>
							<li><a href="https://www.facebook.com/cloudmill/" target="_blank">Facebook</a></li>
							<li><a href="https://www.instagram.com/cloudmill.ru/" target="_blank">Instagram</a></li>
							<li><a href="https://www.behance.net/cloudmill/" target="_blank">Behance</a></li>
						</ul>
					</Col>
				</Row>
				<Row className={'navigation-footer between-xs bottom-xs'}>
					<Col xl={3} sm={4} xs={6} className={'navigation-copy'}><span>©</span> 2021 CloudMill</Col>
					{/* <Col xl={2} sm={3} className={'navigation-link hidden-xs hidden-xs'}><a href="#">Terms of
                        use</a></Col>
						<Col xl={3} sm={4} className={'navigation-link hidden-xs hidden-xs'}><a href="#">Privacy policy</a></Col>  */}
					<Col sm={1} xs={3} className={'navigation-logo'}>
						<a href="https://www.agima.ru/" target={'_blank'}><img src="/assets/images/agima.svg"
							alt=""/></a>
					</Col>
				</Row>
			</div>
		</div>
	);
};

Menu.propTypes = {
	className: PropTypes.string,
	items: PropTypes.array,
	ifClicked: PropTypes.func,
	isHeader: PropTypes.bool,
	handleChange: PropTypes.func
};

export default Menu;
