import React, {Component} from 'react';
import PropTypes from 'prop-types';
import Dropzone from 'react-dropzone';

class Uploader extends Component {
	constructor(props) {
		super(props);
		this.dropHandler = this.dropHandler.bind(this);
	}

	dropHandler(file) {
		return new Promise((resolve, reject) => {
			const req = new XMLHttpRequest();

			req.upload.addEventListener('progress', event => {
				if (event.lengthComputable) {
					console.log(event);
					// const copy = { ...this.state.uploadProgress };
					// copy[file.name] = {
					// 	state: 'pending',
					// 	percentage: (event.loaded / event.total) * 100
					// };
					// this.setState({ uploadProgress: copy });
				}
			});

			req.upload.addEventListener('load', () => {
				// const copy = { ...this.state.uploadProgress };
				// copy[file.name] = { state: 'done', percentage: 100 };
				// this.setState({ uploadProgress: copy });
				console.log(req);
				resolve(req.response);
			});

			req.upload.addEventListener('error', () => {
				// const copy = { ...this.state.uploadProgress };
				// copy[file.name] = { state: 'error', percentage: 0 };
				// this.setState({ uploadProgress: copy });
				console.log(req);
				reject(req.response);
			});

			const formData = new FormData();
			formData.append('file', file, file.name);

			req.open('POST', 'ajax/upload.php');
			req.send(formData);
		});
	}


	render() {
		return (
			<Dropzone
				disableClick={false}
				multiple={false}
				accept={'application/msword, text/plain, application/pdf, image/*'}
				onDrop={this.dropHandler}
			>
				{({getRootProps, getInputProps}) => (
					<section>
						<div {...getRootProps()}>
							<input {...getInputProps()} />
							<p>Drag 'n' drop some files here, or click to select files</p>
						</div>
					</section>
				)}
			</Dropzone>
		);
	}
}

Uploader.propTypes = {
	text: PropTypes.string
};


export default Uploader;
