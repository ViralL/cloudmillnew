import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

// components
// import Picker from 'react-styled-select';

const Select = props => {
	const containerClass = classNames('', props.className, {
		disabled: props.disabled,
		'form-group': props.isGroup
	});
	const inputClass = classNames('select select-fullWidth', {
		'has-error': props.errorMessage
	});
	const errorClass = classNames('text-right', {
		'text-error': props.errorMessage
	});

	return (
		<div className={containerClass}>
			{props.label && <label>{props.label}</label>}
			<div className={inputClass}>
				<select>
					{props.items.map((item, index) => {
						return (
							<option key={index}>
								{item.label}
							</option>
						);
					})}
				</select>
			</div>
			{props.errorMessage && <div className={errorClass}>{props.errorMessage}</div>}
		</div>
	);
};

Select.propTypes = {
	warningMessage: PropTypes.any,
	selectedValue: PropTypes.any,
	errorMessage: PropTypes.any,
	className: PropTypes.string,
	disabled: PropTypes.bool,
	isRequired: PropTypes.bool,
	isGroup: PropTypes.bool,
	clearable: PropTypes.bool,
	searchable: PropTypes.bool,
	label: PropTypes.string,
	id: PropTypes.string,
	defaultValue: PropTypes.object,
	placeholder: PropTypes.string,
	value: PropTypes.string,
	tooltip: PropTypes.string,
	ifChanged: PropTypes.func,
	onInputClear: PropTypes.func,
	ifOpened: PropTypes.func,
	items: PropTypes.array,
	inputRef: PropTypes.any,
	children: PropTypes.any
};

Select.defaultProps = {
	tooltip: '',
	disabled: false,
	isGroup: false,
	searchable: false,
	clearable: false,
	placeholder: 'Select'
};

export default Select;
