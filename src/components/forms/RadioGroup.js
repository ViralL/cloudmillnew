import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

const RadioGroup = props => {
	const containerClass = classNames('radio-group', props.className, {
		disabled: props.disabled,
		'error': props.errorMessage
	});
	const errorClass = classNames('text-left', {
		'text-error': props.errorMessage
	});
	const childrenWithProps = React.Children.map(props.children, child =>
		React.cloneElement(child, { selectedValue: props.selectedValue, disabled: props.disabled })
	);

	return (
		<div className={containerClass}>
			{ props.label && <div className={'radio-group__label'}>{props.label}</div> }
			<div className={'radio-group__inner'}>
				{childrenWithProps.map((item, index) => {
					return (
						<div
							key={index}
							checked={props.selectedValue}
							onChange={props.ifChanged}
							onBlur={props.onFocusOut}
							className={props.radioClassName}
						>
							{item}
						</div>
					);
				})}
			</div>
			{props.errorMessage && <div className={errorClass}>{props.errorMessage}</div>}
		</div>
	);
};

RadioGroup.propTypes = {
	warningMessage: PropTypes.string,
	errorMessage: PropTypes.string,
	className: PropTypes.string,
	radioClassName: PropTypes.string,
	disabled: PropTypes.bool,
	selectedValue: PropTypes.any,
	label: PropTypes.string,
	ifChanged: PropTypes.func,
	onFocusOut: PropTypes.func,
	children: PropTypes.any
};

RadioGroup.defaultProps = {
	disabled: false
};

export default RadioGroup;
