import React, {Component} from 'react';

// components
import Checkbox from './Checkbox';
import Radio from './Radio';
import RadioGroup from './RadioGroup';
import TextInput from './TextInput';
import Select from './Select';

const items = [
	{value: 'chocolate', label: 'Chocolate'},
	{value: 'strawberry', label: 'Strawberry'},
	{value: 'vanilla', label: 'Vanilla'}
];

class Forms extends Component {
	render() {
		return (
			<div>
				<h4>Forms</h4>
				<Select items={items} label={'Label'}/>
				<Select items={items} label={'Label'} errorMessage={'invalidMessage'}/>
				<RadioGroup label={'Label'}>
					<Radio label={'radio option 1'} name={'radio'} id={'radio1_1'}/>
					<Radio label={'radio option 2'} name={'radio'} id={'radio1_2'}/>
					<Radio label={'radio option 3'} name={'radio'} id={'radio1_3'}/>
				</RadioGroup>
				<Checkbox id={'checkbox4'}>
                    Checkbox example with <a href="#">link</a>
				</Checkbox>
				<TextInput label={'Name'} id={'1'} name={'name'} errorMessage={'invalidMessage'}/>
				<TextInput label={'Email'} id={'2'} name={'email'} type={'email'}/>
				<TextInput label={'Login'} id={'3'} icon="profile-male" name={'login'} type={'text'}/>
			</div>
		);
	}

}

export default Forms;
