import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

const TextArea = props => {
	// const containerClass = classNames('input input-fullWidth', props.className, {
	// 	'has-error': props.errorMessage
	// });
	const inputClass = classNames('input input-fullWidth input--nao', {
		'form-group': props.isGroup,
		'input--filled': !!props.value,
	});


	return (
		<div className={inputClass}>
			<textarea
				className="input__field input__field--nao"
				placeholder={props.placeholder}
				name={props.name}
				id={props.id}
				rows={props.rows}
				maxLength={props.maxLength}
				pattern={props.pattern}
				value={props.value}
				readOnly={props.readonly}
				autoComplete={props.autocomplete}
				autoCapitalize={props.autoCapitalize}
				onChange={props.ifChanged}
				onBlur={props.onFocusOut}
				onFocus={props.ifFocussed}
				disabled={props.disabled}
			/>

			<label className="input__label input__label--nao" htmlFor={props.id}>
				<span className="input__label-content input__label-content--nao">{props.label}</span>
			</label>
			<svg className="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
				<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0" />
			</svg>
		</div>
	);
};

TextArea.propTypes = {
	id: PropTypes.string,
	rows: PropTypes.number,
	maxLength: PropTypes.number,
	className: PropTypes.string,
	name: PropTypes.string,
	isRequired: PropTypes.bool,
	label: PropTypes.string,
	children: PropTypes.any,
	pattern: PropTypes.string,
	autoCapitalize: PropTypes.string,
	value: PropTypes.string,
	placeholder: PropTypes.string,
	tooltip: PropTypes.string,
	errorMessage: PropTypes.string,
	autocomplete: PropTypes.string,
	ifFocussed: PropTypes.func,
	onFocusOut: PropTypes.func,
	ifChanged: PropTypes.func,
	ifPressed: PropTypes.func,
	ifClicked: PropTypes.func,
	isEmpty: PropTypes.bool,
	isGroup: PropTypes.bool,
	readonly: PropTypes.bool,
	hasError: PropTypes.bool,
	hasWarning: PropTypes.bool,
	disabled: PropTypes.any
};

TextArea.defaultProps = {
	className: '',
	value: '',
	placeholder: ' ',
	tooltip: '',
	label: '',
	id: '',
	icon: '',
	children: '',
	autocomplete: 'off',
	appear: true,
	hasError: false,
	endTooltip: false,
	isGroup: false,
	readonly: false,
	isEmpty: true
};

export default TextArea;
