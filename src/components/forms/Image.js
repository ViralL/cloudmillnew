import React from 'react';
import PropTypes from 'prop-types';

class Image extends React.Component {
	constructor(props) {
		super(props);
		this.state = { imageStatus: 'loading' };
	}

	handleImageLoaded() {
		this.setState({ imageStatus: 'loaded' });
		this.props.setLoader(true);
	}

	handleImageErrored() {
		this.props.setLoader(true);
		this.setState({ imageStatus: 'failed to load' });
	}

	render() {
		return (
			<>
				<img
					src={this.props.src}
					alt={this.props.alt}
					onLoad={this.handleImageLoaded.bind(this)}
					onError={this.handleImageErrored.bind(this)}
				/>
				{this.state.imageStatus}
			</>
		);
	}
}
Image.propTypes = {
	src: PropTypes.string,
	alt: PropTypes.string,
	setLoader: PropTypes.func,
	loader: PropTypes.bool,
};

Image.defaultProps = {
	setLoader: () => {}
};

export default Image;
