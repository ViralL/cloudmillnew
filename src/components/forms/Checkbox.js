import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

const Checkbox = props => {
	const containerClass = classNames('checkbox checkbox--icon', props.className, {
		'form-group': props.isGroup,
		'text-error': props.error !== undefined,
		disabled: props.disabled
	});

	return (
		<div className={containerClass}>
			<input
				id={props.id}
				type="checkbox"
				name={props.name}
				onChange={props.ifClicked}
				disabled={props.disabled}
				value={props.value}
				// checked={!!props.checked}
			/>
			<label htmlFor={props.id}>
				{props.children || props.label}
			</label>
		</div>
	);
};

Checkbox.propTypes = {
	id: PropTypes.string.isRequired,
	children: PropTypes.any,
	value: PropTypes.oneOfType([
		PropTypes.bool,
		PropTypes.string
	]),
	error: PropTypes.string,
	warning: PropTypes.bool,
	isGroup: PropTypes.bool,
	name: PropTypes.string,
	label: PropTypes.string,
	checked: PropTypes.bool,
	className: PropTypes.string,
	disabled: PropTypes.bool,
	ifChanged: PropTypes.func,
	ifClicked: PropTypes.func
};

Checkbox.defaultProps = {
	className: '',
	value: '',
	id: '',
	isGroup: false,
	ifChanged: null
};

export default Checkbox;
