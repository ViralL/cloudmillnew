import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';


const Radio = props => {
	const containerClass = classNames('radio radio--icon', props.className, {
		disabled: props.disabled
	});
	return (
		<div className={containerClass}>
			<input
				id={props.id}
				type="radio"
				name={props.name}
				onClick={props.onClick}
				onChange={props.onChange}
				onBlur={props.onFocusOut}
				disabled={props.disabled}
				value={props.value}
				readOnly={props.readOnly}
				checked={props.value === props.selectedValue}
			/>
			<label htmlFor={props.id} className={props.labelClass}>
				{props.label || props.children}
			</label>
		</div>
	);
};

Radio.propTypes = {
	id: PropTypes.string,
	label: PropTypes.string,
	value: PropTypes.string,
	selectedValue: PropTypes.any,
	name: PropTypes.string,
	className: PropTypes.string,
	disabled: PropTypes.bool,
	readOnly: PropTypes.bool,
	children: PropTypes.any,
	onChange: PropTypes.func,
	onFocusOut: PropTypes.func,
	labelClass: PropTypes.string,
	checked: PropTypes.bool,
	onClick: PropTypes.func
};

Radio.defaultProps = {
	disabled: false,
	readOnly: true
};

export default Radio;
