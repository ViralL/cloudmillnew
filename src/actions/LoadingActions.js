import { LOADING, LOADING_IMAGE } from '../actions/types';

export const countLoading = (index) => ({
	type: LOADING,
	index
});
export const setLoader = () => ({
	type: LOADING_IMAGE
});
