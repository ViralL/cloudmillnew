import { COMMON_ERROR, POPUPS } from '../Constants';
import { sendMessageOnServer } from '../utils/Server';
import { openPopup } from './PopupActions';
import {
	NAME_CHANGED,
	EMAIL_CHANGED,
	PHONE_CHANGED,
	FILE_CHANGED,
	DESCRIPTION_CHANGED,
	SEND_MESSAGE_FAILURE,
	SEND_MESSAGE,
	SEND_MESSAGE_SUCCESS
} from 'actions/types';


const nameChanged = name => ({
	type: NAME_CHANGED,
	name
});

const emailChanged = email => ({
	type: EMAIL_CHANGED,
	email
});

const phoneChanged = phone => ({
	type: PHONE_CHANGED,
	phone
});

const descriptionChanged = description => ({
	type: DESCRIPTION_CHANGED,
	description
});

const fileChanged = file => ({
	type: FILE_CHANGED,
	file
});

const sendMessageFailure = failure => dispatch => {
	let error;
	window.scrollTo(0, 300);
	switch (failure) {
		case COMMON_ERROR.emptyFields:
			error = COMMON_ERROR.emptyFields;
			break;
		case COMMON_ERROR.wrongEmail:
			error = COMMON_ERROR.wrongEmail;
			break;
		case COMMON_ERROR.wrongPhone:
			error = COMMON_ERROR.wrongPhone;
			break;
		case COMMON_ERROR.technical:
		case COMMON_ERROR.failed:
		default:
			error = COMMON_ERROR.technical;
			break;
	}

	dispatch({
		type: SEND_MESSAGE_FAILURE,
		error
	});
};

const sendMessage = (name, email, phone, description, file) => async dispatch => {
	dispatch({ type: SEND_MESSAGE });
	try {
		// Trim input field values
		const loginValue = name;
		const emailValue = email;
		const phoneValue = phone;
		const descriptionValue = description;

		// Check for empty fields
		if (loginValue === '' || emailValue === '' || phoneValue === '' || descriptionValue === '') {
			throw new Error(COMMON_ERROR.emptyFields);
		}
		// console.log(file);

		// Check if field values pass offline validations
		const emailValid = /^([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})$/i.test(emailValue);
		const phoneValid = /^(\+7\(?\d{3}\)?\d{3} \d{2} \d{2})$/i.test(phoneValue);

		if (!emailValid) {
			throw new Error(COMMON_ERROR.wrongEmail);
		}
		if (!phoneValid) {
			throw new Error(COMMON_ERROR.wrongPhone);
		}

		// Perform the password reset call
		await sendMessageOnServer(name, email, phone, description, file)
			.then((result)=>{
				console.log(result);
			});

		// If the code came this far, the request succeeded
		dispatch({ type: SEND_MESSAGE_SUCCESS });

		setTimeout(() => {
			dispatch(openPopup(POPUPS.messagePopup, '', 'Приняли, уже изучаем. Скоро перезвоним или напишем.'));
		}, 300);
	} catch ({ message }) {
		dispatch(sendMessageFailure(message));
	}
};

export {
	nameChanged,
	fileChanged,
	phoneChanged,
	descriptionChanged,
	emailChanged,
	sendMessage
};
