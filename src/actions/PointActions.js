import {SET_POINT, SET_TAB} from '../actions/types';

const setPoint = (point) => ({
	type: SET_POINT,
	point
});

const setTab = (tab) => ({
	type: SET_TAB,
	tab
});

export {
	setPoint,
	setTab
};
