import {NEWS_REQUEST, SET_NEWS} from '../actions/types';

export function fetchNewsData() {
	return (dispatch) => {
		dispatch({
			type: NEWS_REQUEST,
			value: {
				isLoading: true,
				isError: false
			}
		});
	};
}

export function setCurrentNews(index) {
	return (dispatch) => {
		dispatch({
			type: SET_NEWS,
			index
		});
	};
}
