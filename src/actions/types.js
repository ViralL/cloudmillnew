
export const OPEN_POPUP = 'OPEN_POPUP';
export const CLOSE_POPUP = 'CLOSE_POPUP';

export const LOADING = 'LOADING';
export const LOADING_IMAGE = 'LOADING_IMAGE';
export const SET_TAB = 'SET_TAB';
export const SET_POINT = 'SET_POINT';

export const NEWS_REQUEST = 'NEWS_REQUEST';
export const NEWS_SUCCESS = 'NEWS_SUCCESS';
export const NEWS_FAILURE = 'NEWS_FAILURE';
export const SET_NEWS = 'SET_NEWS';

export const AWARD_REQUEST = 'AWARD_REQUEST';
export const AWARD_SUCCESS = 'AWARD_SUCCESS';
export const AWARD_FAILURE = 'AWARD_FAILURE';

export const SERVICE_REQUEST = 'SERVICE_REQUEST';
export const SERVICE_SUCCESS = 'SERVICE_SUCCESS';
export const SERVICE_FAILURE = 'SERVICE_FAILURE';

export const CATALOG_REQUEST = 'CATALOG_REQUEST';
export const CATALOG_SUCCESS = 'CATALOG_SUCCESS';
export const CATALOG_FAILURE = 'CATALOG_FAILURE';
export const SET_CATALOG_ITEM = 'SET_CATALOG_ITEM';

export const NAME_CHANGED = 'NAME_CHANGED';
export const PHONE_CHANGED = 'PHONE_CHANGED';
export const FILE_CHANGED = 'FILE_CHANGED';
export const EMAIL_CHANGED = 'EMAIL_CHANGED';
export const DESCRIPTION_CHANGED = 'DESCRIPTION_CHANGED';
export const SEND_MESSAGE = 'SEND_MESSAGE';
export const SEND_MESSAGE_SUCCESS = 'SEND_MESSAGE_SUCCESS';
export const SEND_MESSAGE_FAILURE = 'SEND_MESSAGE_FAILURE';
