import { CATALOG_REQUEST, CATALOG_SUCCESS, SET_CATALOG_ITEM } from '../actions/types';
import catalog from '../mocks/catalog';

export function fetchCatalogData(dispatch, params) {
	dispatch({
		type: CATALOG_REQUEST,
		value: {
			isLoading: true,
			isError: false
		}
	});

	try {
		dispatch({
			type: CATALOG_SUCCESS,
			value: {
				catalog,
				isLoading: false,
				isError: false,
			}
		});
		if(params) {
			dispatch({
				type: CATALOG_SUCCESS,
				value: {
					current: params.pathname,
				}
			});
		}
	} catch ({ message }) {
		dispatch({
			type: CATALOG_SUCCESS,
			value: {
				catalog: [],
				isLoading: false,
				isError: true
			}
		});
	}
}

export function setCurrentItem(index) {
	return (dispatch) => {
		dispatch({
			type: SET_CATALOG_ITEM,
			index,
		});
	};
}
