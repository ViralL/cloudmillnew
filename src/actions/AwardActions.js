import { AWARD_REQUEST, AWARD_SUCCESS } from '../actions/types';
import data from '../mocks/awards';

export function fetchAwardsData() {
	return (dispatch) => {
		dispatch({
			type: AWARD_REQUEST,
			value: {
				isLoading: true,
				isError: false
			}
		});
		dispatch({
			type: AWARD_SUCCESS,
			value: {
				awards: data,
				isLoading: false,
				isError: false
			}
		});
	};
}
