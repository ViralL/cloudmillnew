import { OPEN_POPUP, CLOSE_POPUP } from './types';

const openPopup = (popup, title, description, buttons) => ({
	type: OPEN_POPUP,
	popup,
	title,
	description,
	buttons
});

const closePopup = () => ({
	type: CLOSE_POPUP
});

export {
	openPopup,
	closePopup
};
