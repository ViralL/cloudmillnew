import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';
import {bindActionCreators} from 'redux';

import {setCurrentItem, fetchCatalogData} from 'actions/CatalogActions';

import {Helmet} from 'react-helmet';
import {isEmpty} from 'utils/ParseArray';
import LoadDocument from 'components/animate/LoadDocument';
import CardComponent from 'components/catalog/CardComponent';


export class CardDescription extends Component {

	componentDidMount() {
		const pathname = this.props.location.pathname.replace('/projects/', '');
		this.props.setCurrentItem(pathname);
	}

	render() {
		const {current, catalog} = this.props.catalog;
		return (
			<section className="cardContainer">
				<Helmet>
					<link rel="canonical" href={`http://cloudmill.ru/projects/${current}`}/>
				</Helmet>
				{isEmpty(catalog) || current === null
					? <LoadDocument/>
					: <CardComponent item={catalog[current]}/>
				}
			</section>
		);
	}
}

CardDescription.propTypes = {
	fetchCatalogData: PropTypes.func,
	setCurrentItem: PropTypes.func,
	catalog: PropTypes.object,
	location: PropTypes.object
};


const mapStateToProps = (state) => ({
	point: state.point,
	catalog: state.catalog
});

const mapDispatchToProps = dispatch => ({
	setCurrentItem: bindActionCreators(setCurrentItem, dispatch),
	fetchCatalogData: (params) => fetchCatalogData(dispatch, params)
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(CardDescription));
