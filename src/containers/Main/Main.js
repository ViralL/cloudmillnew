import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';
import {Helmet} from 'react-helmet';
import Fade from 'react-reveal/Fade';

// Constants
import {DURATION, URLS, DELAY, POINT, DELAY_ROUTING, TAB, CATALOG_SIZE} from 'Constants';

// components
import ScrollingColorBackground from 'react-scrolling-color-background';
import {Parallax} from 'react-scroll-parallax';
import Row from 'components/grid/Row';
import Col from 'components/grid/Col';
import LoadDocument from 'components/animate/LoadDocument';
import Typography from 'components/markup/Typography';
import CatalogComponent from 'components/catalog/СatalogComponent';
import AwardComponent from 'components/catalog/AwardComponent';
import Linker from 'components/partials/Linker';
import LinkBlock from 'components/partials/LinkBlock';


import {tiltx} from 'utils/TiltxFunction';

export class Main extends Component {
	constructor(props) {
		super(props);
		this.state = {
			rerender: true,
			clientWidth: document.body.clientWidth
		};
	}

	componentDidMount() {
		this.props.fetchAwardsData();
		this.isLoad();
		window.addEventListener('resize', this.setScreenOrientation.bind(this));
		window.addEventListener('orientationchange', this.setScreenOrientation.bind(this));
	}

	componentDidUpdate() {
		!this.props.catalog.isLoading && tiltx();
	}


    setScreenOrientation = () => {
    	setTimeout(() => {
    		this.setState({clientWidth: document.body.clientWidth});
    	}, 100);
    }

    isLoad() {
    	this.setState({
    		rerender: false
    	});
    	setTimeout(() => {
    		this.setState({
    			rerender: true
    		});
    	}, 2000);
    }

    render() {
    	const {point} = this.props.point;
    	const {catalog, isLoading} = this.props.catalog;
    	const {rerender, clientWidth} = this.state;
    	const {awards} = this.props.awards;
    	const size = CATALOG_SIZE;
    	const primeCatalog = Object.entries(catalog).slice(0, size).map(entry => entry[1]);

    	return (
    		<div className={'mainIndexPage'}>
    			<Helmet>
    				<link rel="canonical" href="http://cloudmill.ru"/>
    			</Helmet>
    			{(clientWidth > 1100 && rerender) && <ScrollingColorBackground
    				selector=".js-color-stop[data-background-color]"
    				colorDataAttribute="data-background-color"
    				initialRgb="rgb(8, 21, 183)"
    			/>}
    			<section className="indexPage indexPage-start">
    				<div className={rerender ? 'container js-color-stop relative' : 'container relative'}
    					data-background-color="rgb(255, 255, 255)">
    					<div className={'indexPage-ban'}>
    						<Fade bottom delay={point === POINT.routing ? DELAY_ROUTING : 0} duration={DURATION}>
    							<Parallax className="poster__content" y={[-20, 20]}>
    								<div className="wrapper wrapper_no_padding">
    									<Typography tag={'h1'}>
                                            Digital Production <br/>
                                            c&nbsp;фокусом <br/>
                                            на <img src="/assets/images/fmcg.svg" alt=""/> компании
    									</Typography>
    								</div>
    							</Parallax>
    							<Parallax y={[-15, 15]}>
    								<Typography tag={'p'} className={'text-large'}>
                                        Делаем и&nbsp;развиваем сайты <br/> после запуска
    								</Typography>
    							</Parallax>
    							{clientWidth > 767 ? (
    								<video className={'mainVideo'} autoPlay loop muted>
    									<source src="assets/images/video/logo_5.mp4" type="video/mp4" />
    								</video>
    							) : (
    								<img className={'mainVideo'} src={'/assets/images/video.png'} />
    							)}
    						</Fade>
    						{clientWidth < 768 && <Linker color/>}
    					</div>
    				</div>
    				<div className={'container-long'}>
    					{isLoading
    						? <LoadDocument/>
    						: <CatalogComponent
    							point={point}
    							items={primeCatalog}
    							setCurrentItem={this.props.setCurrentItem}
    						/>
    					}
    				</div>
    				<div className={'container'}>
    					<div className={'col-lg-10 col-xs-12 center-block'}>
    						<Fade bottom duration={DURATION}>
    							<Parallax y={[-20, 20]}>
    								<Typography tag={'h4'} className={'long-link'}>
    									<div className={rerender ? 'js-color-stop ' : ''} data-background-color="rgb(255, 255, 255)"/>
    									<Link
    										to={URLS.case}
    										className={'link'}
    									>
                                            Все проекты
    									</Link>
    									<div className={rerender ? 'js-color-stop' : ''} data-background-color="rgb(8, 21, 183)"/>
    								</Typography>
    							</Parallax>
    						</Fade>
    						<div className={'indexPage-links'}>
    							<Fade bottom duration={DURATION}>
    								<Parallax className={'indexPage-links_item'} y={[-30, 30]}>
    									<Typography tag={'h4'}><a href="http://www.cloudmill.ru/iacm.pdf"
    										target="_blank"
    										className={'link'}>Презентация</a></Typography>
    									<Typography tag={'p'}>
                                            Поможет вам быстро ознакомиться с&nbsp;портфолио <span
    											className="br"/> и&nbsp;начать работу с&nbsp;нами
    									</Typography>
    								</Parallax>
    							</Fade>
    							<Fade bottom delay={DELAY} duration={DURATION}>
    								<Parallax className={'indexPage-links_item'} y={[-30, 30]}>
    									<Typography tag={'h4'}><a
    										href="https://docs.google.com/forms/d/1QUiF8vW4EVNgnRdb77thBDUVoQpTXNgS0cbMBwDmZB8/viewform?edit_requested=true"
    										target="_blank" className={'link'}>Заполнить
                                            бриф</a></Typography>
    									<Typography tag={'p'}>
                                            Мы сможем предложить лучшее решение <span className="br"/> с&nbsp;точной
                                            оценкой
                                            стоимости и&nbsp;сроков
    									</Typography>
    								</Parallax>
    							</Fade>
    						</div>
    					</div>
    				</div>
    			</section>
    			<section className="indexPage indexPage--blue overflow-h">
    				<div className={'container'}>
    					<Parallax y={[-15, 15]}>
    						<Fade bottom duration={DURATION}>
    							<Row className={'between-xs indexPage-menuTitle'}>
    								<Col md={9} xs={12}>
    									<Typography tag={'p'} className={'text--light'} text={'Что мы делаем'}/>
    									<Typography tag={'h2'}>
                                            Помогаем новым <br/> производственным компаниям выйти на&nbsp;рынок,
                                            а&nbsp;компаниям с&nbsp;историей обрести современное лицо в&nbsp;цифровой
                                            среде.
    									</Typography>
    								</Col>
    								<Col md={3} lg={2} xs={12}>
    									<div className={'indexPage-faqLink-body'}><Link to={URLS.contacts}
    										className={'indexPage-faqLink spinIt'}/>
    									</div>
    					 			</Col>
    							</Row>
    						</Fade>
    					</Parallax>
    					<Parallax y={[-20, 20]}>
    						<Row className={rerender ? 'js-color-stop between-xs' : 'between-xs'}
    							data-background-color="rgb(8, 21, 183)">
    							<Col md={4} lg={3} xs={12} className={'last-md'}>
    								<Fade bottom duration={DURATION}>
    									<Typography
    										tag={'p'}
    										className={'text--accent text-right indexPage-menuDesc'}
    									>
											Занимаемся поддержкой,развитием и&nbsp;продвижением
    									</Typography>
    								</Fade>
    							</Col>
    							<Col md={8} lg={9} xs={12}>
    								<Fade bottom cascade duration={DURATION}>
    									<ol className={'indexPage-menu'}>
    										<li><Link to={URLS.services} onClick={() => this.props.setTab(TAB.web)}>Сайты</Link></li>
    										<li><Link to={URLS.services} onClick={() => this.props.setTab(TAB.brand)}>Брендинг</Link></li>
    										<li><Link to={URLS.services} onClick={() => this.props.setTab(TAB.support)}>Поддержка</Link></li>
    									</ol>
    								</Fade>
    							</Col>
    						</Row>
    					</Parallax>
    					<Parallax className={'blue-inside'} y={[-15, 15]}>
    						<Fade bottom duration={DURATION}>
    							<Row className={'between-xs awards'}>
    								<Col md={2} xs={12} className="text-md-left text-center awards-left">
    									<Typography tag={'p'} className={'text--light'} text={'Лента наград'}/>
    									<div className="relative block-inline">
    										<img className={'awards-hand'} src="/assets/images/Robothand.png" alt=""/>
    										<img className={'awards-logo'} src="/assets/images/sign_hand.svg" alt=""/>
    									</div>
    								</Col>
    								<Col md={10} xs={12}>
    									<AwardComponent items={awards}/>
    								</Col>
    							</Row>
    						</Fade>
    					</Parallax>
    					<Parallax className={'indexClients blue-inside'} y={[-15, 15]}>
    						<Fade bottom duration={DURATION}>
    							<Typography tag={'p'} className={'text--light'} text={'Наши клиенты'}/>
    							<Typography tag={'h2'}>
                                    В основном, это компании <br/>
                                    из производственной сферы, которые
                                    стремятся повысить качество
                                    B2B взаимодействия через цифровую среду.
    							</Typography>
    						</Fade>
    						<Fade bottom duration={DURATION}>
    							<ul className={'indexClients-list'}>
    								<li>Чистая Линия</li>
    								<li>МТС.Кассы</li>
    								<li>Молочный завод «Гиагинский»</li>
    								<li>Novikov Group</li>
    								<li>Мытищинский молочный завод</li>
    								<li>Setl Group</li>
    								<li>Ступинский химический завод</li>
    								<li>Агро-Альянс</li>
    								<li>Петербургский Мельничный Комбинат</li>
    								<li>ТКБ Инвестментс</li>
    								<li>Ялуторовский комбинат</li>
    								<li>Театр Балета Бориса Эйфмана</li>
    								<li>Курский молочный завод</li>
    								<li>Первая Мебельная Фабрика</li>
    								<li>ТД «Рублевский»</li>
    								<li>Мосэнергопроект</li>
    							</ul>
    						</Fade>
    					</Parallax>
    				</div>
    			</section>
    			<LinkBlock link={URLS.about} text={'О нас'} class={'indexPage--back'}/>
    		</div>
    	);
    }
}

Main.propTypes = {
	awards: PropTypes.object,
	catalog: PropTypes.object,
	point: PropTypes.object,
	location: PropTypes.object,
	openPopup: PropTypes.func,
	setTab: PropTypes.func,
	setCurrentItem: PropTypes.func,
	fetchCatalogData: PropTypes.func,
	fetchAwardsData: PropTypes.func
};

