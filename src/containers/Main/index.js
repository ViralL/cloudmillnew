import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { bindActionCreators } from 'redux';

import { fetchCatalogData, setCurrentItem } from 'actions/CatalogActions';
import { fetchAwardsData } from 'actions/AwardActions';
import { setTab } from 'actions/PointActions';

import { Main } from './Main';

const mapStateToProps = (state) => ({
	catalog: state.catalog,
	point: state.point,
	awards: state.awards
});

const mapDispatchToProps = dispatch => ({
	setTab: bindActionCreators(setTab, dispatch),
	setCurrentItem: bindActionCreators(setCurrentItem, dispatch),
	fetchCatalogData: (params) => fetchCatalogData(dispatch, params),
	fetchAwardsData: bindActionCreators(fetchAwardsData, dispatch)
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Main));
