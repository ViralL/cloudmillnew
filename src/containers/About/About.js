import React, {Component, Fragment} from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';
import {DELAY, DURATION, POINT, URLS, DELAY_ROUTING} from 'Constants';
import {Parallax} from 'react-scroll-parallax';
import Fade from 'react-reveal/Fade';

// components
import Row from 'components/grid/Row';
import Col from 'components/grid/Col';
import Typography from 'components/markup/Typography';
import AwardComponent from 'components/catalog/AwardComponent';
import LoadDocument from 'components/animate/LoadDocument';
import LinkBlock from 'components/partials/LinkBlock';

import {moveParallax} from 'utils/Parallax';

export class About extends Component {

	componentDidMount() {
		this.props.fetchAwardsData();
	}

	componentDidUpdate() {
		!this.props.awards.isLoading && moveParallax();
	}

	render() {
		const {point} = this.props.point;
		const {awards, isLoading} = this.props.awards;
		return (
			<Fragment>
				<div className={'breadcrumbs'}><Fade right duration={DURATION} delay={point === POINT.routing ? DELAY_ROUTING : 0}>О нас</Fade></div>
				<section className="indexPage about parallax">
					<div className={'container'}>
						<Parallax y={[-10, 10]}>
							<Fade bottom duration={DURATION} delay={point === POINT.routing ? DELAY_ROUTING : 0}>
								<Typography tag={'h2'}>
									Как digital-продакшн, мы&nbsp;фокусируемся <br/>  на&nbsp;качестве результата.
								</Typography>
							</Fade>
						</Parallax>
						<div className={'aboutPico'}>
							<Parallax className={'aboutPico-item'} y={[-20, 20]}>
								<Fade bottom duration={DURATION} delay={point === POINT.routing ? DELAY_ROUTING : 0}>
									<div className={'aboutPico-img'}>
										<span>Product</span>
										<div className="aboutPico-icons">
											<img src="/assets/images/about/1/icon_1_part.svg"
												className="parallax-layer parallax-layer__2" alt=""
												data-parallax-deep="100"/>
											<img src="/assets/images/about/1/icon_1_part2.svg"
												className="parallax-layer parallax-layer__2" alt=""
												data-parallax-deep="120"/>
										</div>
									</div>
									<Typography tag={'p'}>
										Изучаем продукт<br/>и&nbsp;его целевые группы
									</Typography>
								</Fade>
							</Parallax>
							<Parallax className={'aboutPico-item'} y={[-20, 20]}>
								<Fade bottom delay={point === POINT.routing ? DELAY_ROUTING : DELAY} duration={DURATION}>
									<div className={'aboutPico-img parallax-layer parallax-layer__2'}>
										<span>Interraction</span>
										<div className="aboutPico-icons">
											<img src="/assets/images/about/2/icon_2_part_1.svg" alt=""
												className="parallax-layer parallax-layer__2" alt=""
												data-parallax-deep="120"/>
											<img src="/assets/images/about/2/icon_2_part_2.svg" alt=""
												className="parallax-layer parallax-layer__2" alt=""
												data-parallax-deep="130"/>
										</div>
									</div>
									<Typography tag={'p'}>
										Проектируем <br/> взаимодействия
									</Typography>
								</Fade>
							</Parallax>
							<Parallax className={'aboutPico-item'} y={[-20, 20]}>
								<Fade bottom delay={point === POINT.routing ? DELAY_ROUTING : DELAY * 1.5} duration={DURATION}>
									<div className={'aboutPico-img parallax-layer parallax-layer__3'}>
										<span>Communication</span>
										<div className="aboutPico-icons">
											<img src="/assets/images/about/3/icon_3_part_1.svg" alt=""
												className="parallax-layer parallax-layer__2" alt=""
												data-parallax-deep="115"/>
											<img src="/assets/images/about/3/icon_3_part_2.svg" alt=""
												className="parallax-layer parallax-layer__2" alt=""
												data-parallax-deep="125"/>
										</div>
									</div>
									<Typography tag={'p'}>
										Подбираем тон <br/> коммуникации
									</Typography>
								</Fade>
							</Parallax>
							<Parallax className={'aboutPico-item'} y={[-20, 20]}>
								<Fade bottom delay={point === POINT.routing ? DELAY_ROUTING : DELAY * 2} duration={DURATION}>
									<div className={'aboutPico-img'}>
										<span>Photo<br/>
										Video</span>
										<div className="aboutPico-icons">
											<img src="/assets/images/about/4/icon_4_part_1.svg" alt=""
												className="parallax-layer parallax-layer__2" alt=""
												data-parallax-deep="125"/>
											<img src="/assets/images/about/4/icon_4_part_2.svg" alt=""
												className="parallax-layer parallax-layer__2" alt=""
												data-parallax-deep="135"/>
										</div>
									</div>
									<Typography tag={'p'}>
										Проводим <br/>фото- и&nbsp;видеосъемку
									</Typography>
								</Fade>
							</Parallax>
						</div>
						<Parallax y={[-20, 20]}>
							<div className={'aboutBitrix'}>
								<Fade bottom duration={DURATION} delay={point === POINT.routing ? DELAY_ROUTING / 2 : 0}>
									<div className={'aboutBitrix-txt'}>Золотой сертифицированный <br/> партнер 1С-Битрикс</div>
									<div className={'aboutBitrix-txt'}>Участник программы мониторинга <br/> качества внедрений</div>
									<img src="/assets/images/about/bitrix.svg" className={'aboutBitrix-icon'} alt=""/>
									<div className={'aboutBitrix-img'}><img src="/assets/images/about/women.png" alt=""/></div>
								</Fade>
							</div>
						</Parallax>
						<Parallax className={'aboutSite'} y={[-5, 5]}>
							<Fade bottom duration={DURATION}>
								<Typography tag={'h2'}>
                                    В рамках развития проектов <br/>
                                    поможем внести правки на&nbsp;сайт, <br/>
                                    провести интеграцию с&nbsp;внешними <br/>
                                    сервисами и&nbsp;автоматизацию процессов.
								</Typography>
							</Fade>
							<Row className={'between-xs'}>
								<Col lg={5} md={6} xs={12}>
									<Fade bottom duration={DURATION}>
										<ul className={'aboutSite-list'}>
											<li>Нашу разработку в&nbsp;дальнейшем легко передать для развития,
                                                а&nbsp;рекламные кампании, при желании, вести внутри собственной
                                                компании.
											</li>
										</ul>
										<div className={'text-center'}>
											<div className={'indexPage-faqLink-body'}><Link to={URLS.contacts} className={'indexPage-faqLink spinIt'}/></div>
										</div>
									</Fade>
								</Col>
								<Col lg={5} md={6} xs={12}>
									<Fade cascade bottom duration={DURATION}>
										<ul className={'aboutSite-list'}>
											<li>Всегда передаем доступы <br/> от&nbsp;рекламных кампаний</li>
											<li>Все сайты проходят программу <br/> «монитор качества» 1С-Битрикс</li>
											<li>Строгая отчетность <br/> по&nbsp;проделанным работам</li>
										</ul>
									</Fade>
								</Col>
							</Row>
						</Parallax>
						<Parallax y={[-15, 15]}>
							<Fade bottom duration={DURATION}>
								<Row className={'between-xs awards'}>
									<Col md={2} xs={12} className="text-md-left text-center">
										<Typography tag={'p'} className={'text--light'} text={'Лента наград'}/>
										<div className="relative block-inline">
											<img className={'awards-hand'} src="/assets/images/Robothand.png" alt=""/>
											<img className={'awards-logo'} src="/assets/images/sign_hand.svg" alt=""/>
										</div>
									</Col>
									<Col md={10} xs={12}>
										{isLoading
											? <LoadDocument/>
											: <AwardComponent items={awards}/>
										}
									</Col>
								</Row>
							</Fade>
						</Parallax>
						<Parallax className={'indexClients'} y={[-15, 15]}>
							<Fade bottom duration={DURATION}>
								<Typography tag={'p'} className={'text--light'} text={'Наши клиенты'}/>
								<Typography tag={'h2'}>
										В основном, это компании <br/>
										из производственной сферы, которые
										стремятся повысить качество
										B2B взаимодействия через цифровую среду.
								</Typography>
							</Fade>
							<Fade bottom duration={DURATION}>
								<ul className={'indexClients-list'}>
									<li>Чистая Линия</li>
									<li>МТС.Кассы</li>
									<li>Молочный завод «Гиагинский»</li>
									<li>Novikov Group</li>
									<li>Мытищинский молочный завод</li>
									<li>Setl Group</li>
									<li>Ступинский химический завод</li>
									<li>Агро-Альянс</li>
									<li>Петербургский Мельничный Комбинат</li>
									<li>ТКБ Инвестментс</li>
									<li>Ялуторовский комбинат</li>
									<li>Театр Балета Бориса Эйфмана</li>
									<li>Курский молочный завод</li>
									<li>Первая Мебельная Фабрика</li>
									<li>ТД «Рублевский»</li>
									<li>Мосэнергопроект</li>
								</ul>
							</Fade>
						</Parallax>
					</div>
				</section>
				<LinkBlock link={URLS.approach} text={'Наш подход'} class={'indexPage--backApp'}/>
			</Fragment>
		);
	}
}

About.propTypes = {
	awards: PropTypes.object,
	point: PropTypes.object,
	catalog: PropTypes.object,
	location: PropTypes.object,
	openPopup: PropTypes.func,
	fetchCatalogData: PropTypes.func,
	fetchAwardsData: PropTypes.func
};

