import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';
import {bindActionCreators} from 'redux';

import {fetchCatalogData, setCurrentItem} from 'actions/CatalogActions';

import {Case} from './Case';

const mapStateToProps = (state) => ({
	point: state.point,
	catalog: state.catalog
});

const mapDispatchToProps = dispatch => ({
	setCurrentItem: bindActionCreators(setCurrentItem, dispatch),
	fetchCatalogData: (params) => fetchCatalogData(dispatch, params)
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Case));
