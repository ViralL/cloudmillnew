import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {DELAY, DURATION, POINT, URLS, DELAY_ROUTING, CATALOG_SIZE} from 'Constants';
import {Helmet} from 'react-helmet';
import {Parallax} from 'react-scroll-parallax';

// components
import Typography from 'components/markup/Typography';
import CatalogComponent from 'components/catalog/СatalogComponent';
import LoadDocument from 'components/animate/LoadDocument';
import LinkBlock from 'components/partials/LinkBlock';

import {tiltx} from 'utils/TiltxFunction';
import Fade from 'react-reveal/Fade';

export class Case extends Component {

	componentDidUpdate() {
    	!this.props.catalog.isLoading && tiltx();
	}

	render() {
    	const {point} = this.props.point;
    	const {catalog, isLoading} = this.props.catalog;
    	const size = CATALOG_SIZE;
    	const primeCatalog = Object.entries(catalog).slice(0, size).map(entry => entry[1]);

    	return (
    		<>
    			<Helmet>
    				<title>Проекты | CloudMill Digital Production</title>
    				<link rel="canonical" href="http://cloudmill.ru/project"/>
    			</Helmet>
    			<div className={'breadcrumbs'}><Fade right duration={DURATION}
    				delay={point === POINT.routing ? DELAY_ROUTING : 0}>Проекты</Fade>
    			</div>
    			<section className="indexPage filter indexPage--white">
    				<div className={'container-long'}>
    					{isLoading
    						? <LoadDocument/>
    						: <CatalogComponent
    							point={point}
    							items={primeCatalog}
    							setCurrentItem={this.props.setCurrentItem}
    						/>
    					}
    				</div>
    				<div className={'container'}>
    					<div className={'col-lg-10 col-xs-12 center-block'}>
    						<div className={'indexPage-links'}>
    							<Fade bottom duration={DURATION}>
    								<Parallax className={'indexPage-links_item'} y={[-30, 30]}>
    									<Typography tag={'h4'}><a href="http://www.cloudmill.ru/iacm.pdf"
    										target="_blank"
    										className={'link'}>Презентация</a></Typography>
    									<Typography tag={'p'}>
                                            Поможет вам быстро ознакомиться с&nbsp;портфолио <br/> и&nbsp;начать работу
                                            с&nbsp;нами
    									</Typography>
    								</Parallax>
    							</Fade>
    							<Fade bottom delay={DELAY} duration={DURATION}>
    								<Parallax className={'indexPage-links_item'} y={[-30, 30]}>
    									<Typography tag={'h4'}><a
    										href="https://docs.google.com/forms/d/1QUiF8vW4EVNgnRdb77thBDUVoQpTXNgS0cbMBwDmZB8/viewform?edit_requested=true"
    										target="_blank" className={'link'}>Заполнить
                                            бриф</a></Typography>
    									<Typography tag={'p'}>
                                            Мы сможем предложить лучшее решение <br/> с&nbsp;точной оценкой стоимости
                                            и&nbsp;сроков
    									</Typography>
    								</Parallax>
    							</Fade>
    						</div>
    					</div>
    				</div>
    			</section>
    			<LinkBlock
    				link={URLS.contacts}
    				text={'Контакты'}
    				class={'indexPage--blue indexPage--contacts'}
    			/>
    		</>
    	);
	}
}

Case.propTypes = {
	isLoading: PropTypes.bool,
	point: PropTypes.object,
	catalog: PropTypes.object,
	location: PropTypes.object,
	setCurrentItem: PropTypes.func,
	fetchCatalogData: PropTypes.func
};


