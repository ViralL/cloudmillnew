import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { bindActionCreators } from 'redux';

import {
	sendMessage,
	nameChanged,
	phoneChanged,
	fileChanged,
	descriptionChanged,
	emailChanged
} from 'actions/ContactsActions';

import { Contacts } from './Contacts';

const mapStateToProps = (state) => ({
	point: state.point,
	contacts: state.contacts
});

const mapDispatchToProps = dispatch => ({
	sendMessage: bindActionCreators(sendMessage, dispatch),
	fileChanged: bindActionCreators(fileChanged, dispatch),
	nameChanged: bindActionCreators(nameChanged, dispatch),
	phoneChanged: bindActionCreators(phoneChanged, dispatch),
	emailChanged: bindActionCreators(emailChanged, dispatch),
	descriptionChanged: bindActionCreators(descriptionChanged, dispatch)
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Contacts));
