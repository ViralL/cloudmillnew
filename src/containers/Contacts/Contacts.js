import React, {Component, Fragment} from 'react';
import PropTypes from 'prop-types';
import Fade from 'react-reveal/Fade';
import {menu} from 'mocks/menu';

import {DELAY, DURATION, POINT, DELAY_ROUTING} from 'Constants';

// components
import Typography from 'components/markup/Typography';
import TextInput from 'components/forms/TextInput';
import PhoneInput from 'components/forms/PhoneInput';
import TextArea from 'components/forms/TextArea';
import Row from 'components/grid/Row';
import Col from 'components/grid/Col';
import Button from 'components/buttons/Button';
import LoadDocument from 'components/animate/LoadDocument';
import Menu from 'components/partials/Menu';

export class Contacts extends Component {
	constructor(props) {
		super(props);
		this.state = {
			file: null
		};
		this.onChange = this.onChange.bind(this);
	}

	onChange(e) {
		this.setState({file: e.target.files[0]});
	}

	render() {
		const {sendMessage, nameChanged, phoneChanged, descriptionChanged, emailChanged, contacts, point} = this.props;
		const { file } = this.state;
		const {name, phone, email, description, error, loading} = contacts;

		return (
			<Fragment>
				<div className={'breadcrumbs'}><Fade right duration={DURATION}
					delay={point.point === POINT.routing ? DELAY_ROUTING : 0}>Контакты</Fade>
				</div>
				<section className="indexPage contacts">
					<div className={'container'}>
						<div className={'col-lg-10 center-block'}>
							<Fade bottom duration={DURATION} delay={point.point === POINT.routing ? DELAY_ROUTING : 0}>
								<Typography tag={'h2'}>
                                    Расскажите нам о&nbsp;своем проекте, подумаем над ним вместе. Начните
                                    с&nbsp;простого — просто напишите нам.
								</Typography>
							</Fade>
							<Fragment>
								{error && <div className={'text-right text--accent h5'}>{error}</div>}
								<Fade bottom duration={DURATION}
									delay={point.point === POINT.routing ? DELAY_ROUTING : 0}>
									<TextInput
										isGroup
										label={'Вас зовут'}
										id={'name'}
										name={'name'}
										value={name}
										ifChanged={(event) => nameChanged(event.target.value)}
									/>
								</Fade>
								<Fade bottom duration={DURATION}
									delay={point.point === POINT.routing ? DELAY_ROUTING : DELAY}>
									<PhoneInput
										isGroup
										label={'Телефон для связи'}
										id={'phone'}
										name={'phone'}
										value={phone}
										ifChanged={(event) => phoneChanged(event.target.value)}
									/>
								</Fade>
								<Fade bottom duration={DURATION}>
									<TextInput
										isGroup
										label={'Почта'}
										id={'email'}
										value={email}
										name={'email'}
										ifChanged={(event) => emailChanged(event.target.value)}
									/>
								</Fade>
								<Fade bottom duration={DURATION}>
									<TextArea
										isGroup
										rows={3}
										label={'Какой проект обсуждаем?'}
										id={'desc'}
										value={description}
										name={'desc'}
										ifChanged={(event) => descriptionChanged(event.target.value)}
									/>
								</Fade>
								<Fade bottom duration={DURATION}>
									<Row className={'between-xs middle-xs'}>
										<Col sm={4} xs={12} className={'text-center-xs'}>
											<div className={'file-input magnetize'}>
												<input type="file" onChange={this.onChange}/>
												<span>{file ? file.name : 'Прикрепить файл +'}</span>
											</div>
										</Col>
										<Col sm={8} xs={12} className={'text-right-md text-center-xs'}>
											<Button
												className="magnetize"
												isTransparent
												disabled={loading}
												ifClicked={() => sendMessage(name, email, phone, description, file)}
											>
												{loading ? <LoadDocument white/> : <span>Let’s go!</span>}
											</Button>
										</Col>
									</Row>
								</Fade>
							</Fragment>
						</div>
					</div>
					<Fade bottom duration={DURATION}>
						<div className={'contacts-info'}>
							<Menu items={menu}/>
						</div>
					</Fade>
				</section>
			</Fragment>
		);
	}
}

Contacts.propTypes = {
	isLoading: PropTypes.bool,
	point: PropTypes.object,
	contacts: PropTypes.object,
	location: PropTypes.object,
	sendMessage: PropTypes.func,
	fileChanged: PropTypes.func,
	nameChanged: PropTypes.func,
	phoneChanged: PropTypes.func,
	emailChanged: PropTypes.func,
	descriptionChanged: PropTypes.func
};


