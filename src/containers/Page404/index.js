import { withRouter } from 'react-router-dom';
import {connect} from 'react-redux';

import { Page404 } from './Page404';

const mapStateToProps = (state) => ({
	point: state.point
});

export default withRouter(connect(mapStateToProps, [])(Page404));
