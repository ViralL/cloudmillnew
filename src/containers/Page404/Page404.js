import React, {Component, Fragment} from 'react';
import classNames from 'classnames';
import {Link} from 'react-router-dom';
import {menu404} from 'mocks/menu';
import {Helmet} from 'react-helmet';
import PropTypes from 'prop-types';
import Fade from 'react-reveal/Fade';

// components
import Typography from 'components/markup/Typography';
import {DELAY, DURATION, POINT, DELAY_ROUTING} from 'Constants';

export class Page404 extends Component {
	render() {
		const {point} = this.props.point;
		return (
			<Fragment>
				<Helmet>
					<title>Страница не найдена | CloudMill Digital Production</title>
					<link rel="canonical" href="http://cloudmill.ru/page404"/>
				</Helmet>
				<div className={'breadcrumbs'}><Fade right duration={DURATION} delay={point === POINT.routing ? DELAY_ROUTING : 0}>404</Fade></div>
				<section className="indexPage page404">
					<div className={'container'}>
						<Fade bottom duration={DURATION}>
							<Typography
								tag={'h2'}
							>
                            Ты потерялся, друг :( <br/>
                            Продолжи свой путь <br/>
                            где-нибудь там:
							</Typography>
						</Fade>
						<Fade bottom cascade  delay={point === POINT.routing ? DELAY_ROUTING : DELAY} duration={DURATION}>
							<ul className={'menu'}>
								{menu404.map((item, index) => {
									const itemClasses = classNames('menu-item');
									return (
										<li className={itemClasses} key={index}>
											<Link
												to={item.link}
												className="menu-link"
											>
												{item.text}
											</Link>
										</li>
									);
								})}
							</ul>
						</Fade>
					</div>
				</section>
			</Fragment>
		);
	}
}

Page404.propTypes = {
	point: PropTypes.object
};
