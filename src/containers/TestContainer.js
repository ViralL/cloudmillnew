import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';
import {bindActionCreators} from 'redux';

import {fetchCatalogData} from 'actions/CatalogActions';
// import rotation from 'mocks/rotation';

// components
// import Forms from 'components/forms/index';
import Buttons from 'components/buttons/index';
import Markup from 'components/markup/index';
import Tabs from 'components/tabs/Tabs';
import Uploader from 'components/forms/Uploader';
// import RotationComponent from 'components/animate/Rotation';

// import {Sketch} from 'utils/Sketch';

class TestContainer extends Component {

	componentDidMount() {
		const pathname = this.props.location.pathname.replace('/projects/', '');
		this.props.fetchCatalogData({pathname});
	}

	componentDidUpdate() {
		// !this.props.catalog.isLoading && Sketch();
	}

	// changeImg(e) {
	// 	this.setState({
	// 		state: e
	// 	});
	// }

	render() {
		return (
			<section className={'container'}>
				<br/>
				<br/>
				<br/>
				<br/>
				<br/>
				<Uploader text={'Загрузить'}/>
				<Tabs>
					<div label={'Point of delivery'}>
						<h2>Point of delivery</h2>
					</div>
					<div label={'Tab'}>
						<h2>Tab</h2>
					</div>
				</Tabs>
				{/* <div className="frame">
					<div
						id="gl"
						data-imageoriginal="/assets/images/lady.jpg"
						data-imagedepth="/assets/images/lady-map.jpg"
						data-horizontalthreshold="35"
						data-verticalthreshold="15"/>
				</div> */}
				{/* <RotationComponent items={rotation} ifChanged={(event) => this.changeImg(event)}/> */}
				{/* <Forms/> */}
				<Buttons/>
				<Markup/>
			</section>
		);
	}
}

TestContainer.propTypes = {
	fetchCatalogData: PropTypes.func,
	catalog: PropTypes.object,
	location: PropTypes.object,
};


const mapStateToProps = (state) => ({
	catalog: state.catalog
});

const mapDispatchToProps = dispatch => ({
	fetchCatalogData: (params) => fetchCatalogData(dispatch, params),
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(TestContainer));

