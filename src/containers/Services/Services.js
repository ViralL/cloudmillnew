import React, {Component} from 'react';
import PropTypes from 'prop-types';
import Fade from 'react-reveal/Fade';

// Constants
import {CATALOG_SIZE, DELAY_ROUTING, DURATION, POINT, TAB, URLS} from 'Constants';

// components
import Typography from 'components/markup/Typography';
import LinkBlock from 'components/partials/LinkBlock';
import Tabs from 'components/tabs/Tabs';
import Row from 'components/grid/Row';
import Col from 'components/grid/Col';
import LoadDocument from 'components/animate/LoadDocument';
import CatalogComponent from 'components/catalog/СatalogComponent';


export class Services extends Component {
	constructor(props) {
		super(props);
		this.state = {
			hover: false
		};
	}

	toggleHoverEnter() {
		this.setState({
			hover: true
		});
	}
	toggleHoverOver() {
		this.setState({
			hover: false
		});
	}

	render() {
		const {hover} = this.state;
		const {point} = this.props.point;
		const {catalog, isLoading} = this.props.catalog;
		const size = CATALOG_SIZE;
		const primeCatalog = Object.entries(catalog).slice(0, size).map(entry => entry[1]);

		return (
			<>
				<div className={'breadcrumbs'}><Fade right duration={DURATION}
					delay={point === POINT.routing ? DELAY_ROUTING : 0}>Услуги</Fade>
				</div>
				<section className="indexPage servicePage">
					<Tabs point={point}>
						<div label={TAB.web}>
							<Fade bottom duration={DURATION} delay={point === POINT.routing ? DELAY_ROUTING : 0}>
								<div className={'container services'}>
									<div className={'col-xl-10 col-lg-11 center-block'}>
										<Typography tag={'h5'} className="servicesTxt-item">
											<div><span>Наша ниша — FMCG и сайты производителей</span>.</div>
											<br/>
                                            Разрабатываем корпоративные сайты <br/>
                                            для B2B взаимодействия, <br/>
                                            яркие промо-решения для B2C, <br/>
                                            проектируем личные кабинеты, <br/>
                                            интегрируем программы лояльности.
										</Typography>
									</div>
								</div>
							</Fade>
							<div className={'services-gray services'}>
								<Fade bottom duration={DURATION} delay={point === POINT.routing ? DELAY_ROUTING : 0}>
									<div className={'container'}>
										<div className={'col-xl-10 col-lg-11 center-block relative'}>
											<div className={'servicesTitle'}>
												<div className={'servicesTitle-item'}>
													<Typography tag={'h2'} text={'Изучаем'}/>
													<Typography tag={'p'} text={'конкурентов, целевую аудиторию.'}/>
												</div>
												<div className={'servicesTitle-item'}>
													<Typography tag={'h2'} text={'Выявляем'}/>
													<Typography tag={'p'} text={'боли, страхи, барьеры.'}/>
												</div>
											</div>
											<div className={'servicesTitle--red'}>
												<div className={'servicesTitle-item'}>
													<Typography tag={'h2'}>
                                                        Проектируем<br/>интерфейсы
													</Typography>
												</div>
											</div>
											<div className={'servicesList-icon'}><img
												src={'/assets/images/services/circle.svg'} alt=""/></div>
										</div>
									</div>
								</Fade>
								<div className={'container-long'}>
									<Fade bottom duration={DURATION}>
										<div className={'servicesSeo'}>
											<Typography tag={'h2'} text={'Сайты SEO-подготовлены'}/>
											<Row className={'servicesSeo-item between-xs'}>
												<Col md={6} xs={12}>
													<Typography tag={'h5'}>
                                                        SEO-подготовка у нас <br/>
                                                        это не просто создание <br/>
                                                        и заполнение по шаблону мета-данных.
													</Typography>
													<img src="/assets/images/services/arr.svg" alt=""/>
												</Col>
												<Col md={5} xs={12}>
													<ul className="servicesSeo-list">
														<li>Сбор семантики</li>
														<li>Анализ поисковых запросов</li>
														<li>Последующая кластеризация</li>
													</ul>
												</Col>
											</Row>
											<Row className={'servicesSeo-item between-xs bottom-xs'}>
												<Col md={7} xs={12}>
													<Typography tag={'h5'}>
                                                        Структура сайта <br/> проектируется, опираясь <br/> на данные
                                                        анализа.
													</Typography>
												</Col>
												<Col md={5} xs={12}>
													<Typography tag={'h2'} text={'Итог:'}/>
													<p>проекты выбирают целевой трафик по-максимуму.</p>
												</Col>
											</Row>
										</div>
									</Fade>
									<Fade bottom duration={DURATION}>
										<div className={'servicesForm'}>
											<Typography tag={'h2'}>
                                                Содержание <br/>
                                                диктует <br/>
                                                форму
											</Typography>
											<Row className={'end-xs'}>
												<Col lg={6} md={7} xs={12}>
													<Typography tag={'h6'} className="servicesTxt-item">
														<div><span>Полнота и осмысленность контента определяют дальнейшую подачу.</span>
														</div>
														<br/>
                                                        Работая над улучшением взаимодействия
                                                        и&nbsp;удержанием пользователей, мы выделяем основной смысл,
                                                        подчеркиваем самое важное, работаем с клиентскими инсайтами.
													</Typography>
													<Typography tag={'p'}>
                                                        Один из этапов разработки — «инфо-прототипирование». <br/>
                                                        Он помогает правильно структурировать информацию, определить
                                                        иерархию и «вес» контента.
													</Typography>
												</Col>
											</Row>
											<img src="/assets/images/services/item-7.svg" alt=""/>
										</div>
									</Fade>
									<Fade bottom duration={DURATION}>
										<div className={'servicesDev'}>
											<img src="/assets/images/services/item-8.svg" alt=""/>
											<Typography tag={'h2'}>
                                                Разработка
											</Typography>
											<Col lg={9} xs={12}>
												<Typography tag={'h5'} className="servicesTxt-item">
                                                    Мы используем CMS «1С-Битрикс»
                                                    и&nbsp;ежегодно подтверждаем статус&nbsp;<br/>
													<span>золотого сертифицированного партнера</span>.
												</Typography>
												<Typography tag={'p'} className="servicesTxt-item">
                                                    Все проекты в обязательном порядке проходят программу
                                                    «мониторинг <br/> качества».&nbsp;
													<span>Занимаемся сайтами 10 лет и&nbsp;многое повидали</span>.<br/>
                                                    Беремся за&nbsp;проекты со&nbsp;сложной информационной архитектурой,<br/>
                                                    интеграцией с&nbsp;CRM, ERP, внешними сервисами и базами данных.
												</Typography>
											</Col>
										</div>
									</Fade>
								</div>
							</div>
						</div>
						<div label={TAB.brand}>
							<Fade bottom duration={DURATION} delay={point === POINT.routing ? DELAY_ROUTING : 0}>
								<div className={'container services'}>
									<div className={'col-xl-10 col-lg-11 center-block'}>
										<Fade bottom duration={DURATION}>
											<Typography tag={'h5'} className="servicesTxt-item">
                                                Создадим <span>целостный образ вашего бренда</span>. Правильный симбиоз
                                                айдентики и веб-дизайна.
											</Typography>
										</Fade>
										<Fade bottom cascade duration={DURATION}>
											<div className={'servicesList'}>
												<div className={'servicesList-item'}><img
													src={'/assets/images/services/item-1.svg'} alt=""/></div>
												<div className={'servicesList-item'}><img
													src={'/assets/images/services/item-2.svg'} alt=""/></div>
												<div className={'servicesList-item'}><img
													src={'/assets/images/services/item-3.svg'} alt=""/></div>
												<div
													className={'servicesList-item'}

												>
													<img
														onMouseEnter={() => this.toggleHoverEnter()}
														onMouseLeave={() => this.toggleHoverOver()}
														src={!hover
															? '/assets/images/services/item-4.png'
															: '/assets/images/services/service.gif'}
														alt=""
													/>
												</div>
												<div className={'servicesList-item'}><img
													src={'/assets/images/services/item-5.svg'} alt=""/></div>
												<div className={'servicesList-item'}><img
													src={'/assets/images/services/item-6.svg'} alt=""/></div>
											</div>
										</Fade>
									</div>
								</div>
							</Fade>
							<div className={'services-gray services'}>
								<Fade bottom cascade duration={DURATION}>
									<div className={'container'}>
										<div className={'col-xl-10 col-lg-11 center-block relative'}>
											<div className={'servicesTitle'}>
												<div className={'servicesTitle-item'}>
													<Typography tag={'h2'} text={'Изучаем'}/>
													<Typography tag={'p'}
														text={'рынок, конкурентов, покупательские предпочтения.'}/>
												</div>
												<div className={'servicesTitle-item'}>
													<Typography tag={'h2'} text={'Учитываем'}/>
													<Typography tag={'p'}
														text={'цели компании, особенности бизнеса, корпоративную культуру.'}/>
												</div>
											</div>
											<div className={'servicesTitle--red servicesTitle--red-left'}>
												<div className={'servicesTitle-item'}>
													<Typography tag={'h3'}>
                                                        Создаем мощный инструмент
                                                        для коммуникации
                                                        с&nbsp;вашим потребителем
													</Typography>
												</div>
											</div>
											<div className={'servicesList-icon2'}><img
												src={'/assets/images/services/circle.svg'} alt=""/></div>
										</div>
									</div>
								</Fade>
							</div>
						</div>
						<div label={TAB.support}>
							<Fade bottom cascade duration={DURATION}
								delay={point === POINT.routing ? DELAY_ROUTING : 0}>
								<div className={'container services'}>
									<div className={'col-xl-10 col-lg-11 center-block'}>
										<Typography tag={'h5'} className="servicesTxt-item">
                                            Возьмем на поддержку ваш интернет-проект <br/>
                                            и&nbsp;будем <span>развивать его согласно требованиям <br/> бизнеса и результатам аналитики</span>.<br/>
                                            Мы предлагаем запускать продукт и итеративно <br/> его улучшать, совмещая
                                            работы по сайту <br/>
                                            с внутренними проектами вашей компании.
										</Typography>
									</div>
								</div>
							</Fade>
							<Fade bottom cascade duration={DURATION}
								delay={point === POINT.routing ? DELAY_ROUTING : 0}>
								<div className={'services-gray services-gray--lines services'}>
									<div className={'container'}>
										<div className={'col-xl-10 col-lg-11 center-block relative'}>
											<div className={'servicesTitle'}>
												<div className={'servicesTitle-item'}>
													<Typography tag={'h2'}>
                                                        Развитие<br/>
                                                        и тех.поддержка<br/>
                                                        с вменяемым SLA
													</Typography>
													<Typography tag={'h6'}>
                                                        Современный маркетинг — это продуктовая работа<br/>
                                                        с непрерывной проверкой гипотез. Чтобы эти гипотезы<br/>
                                                        проверять требуется постоянная работа над сайтом.
													</Typography>
												</div>
											</div>
										</div>
									</div>
								</div>
							</Fade>
							<Fade bottom cascade duration={DURATION}>
								<div className={'container servicesLists'}>
									<Row className={'between-xs servicesLists-item'}>
										<Col md={6} xs={12}>
											<Typography tag={'h2'}>
                                                В рамках<br/>
                                                поддержки:
											</Typography>
										</Col>
										<Col md={5} xs={12}>
											<ul className="servicesLists-list">
												<li>Изучаем метрики</li>
												<li>Анализируем пользовательское поведение</li>
												<li>Оцениваем эффективность интернет-маркетинга</li>
											</ul>
										</Col>
									</Row>
									<Row className={'between-xs servicesLists-item'}>
										<Col md={6} xs={12}>
											<Typography tag={'h2'}>
                                                На основе<br/>
                                                данных:
											</Typography>
										</Col>
										<Col md={5} xs={12}>
											<ul className="servicesLists-list">
												<li>Внедряем улучшения</li>
												<li>Автоматизируем процессы</li>
												<li>Интегрируем внешние сервисы: <br/><span>CRM, ERP, сервисы оплаты, службы доставки</span>
												</li>
											</ul>
										</Col>
									</Row>
								</div>
							</Fade>
						</div>
					</Tabs>
					<div className={'container-long'}>
						{isLoading
							? <LoadDocument/>
							: <CatalogComponent
								point={point}
								items={primeCatalog}
								setCurrentItem={this.props.setCurrentItem}
							/>
						}
					</div>
				</section>
				<LinkBlock link={URLS.contacts} text={'Контакты'} class={'indexPage--blue indexPage--contacts'}/>
			</>
		);
	}
}

Services.propTypes = {
	isLoading: PropTypes.bool,
	point: PropTypes.object,
	catalog: PropTypes.object,
	location: PropTypes.object,
	setCurrentItem: PropTypes.func,
	fetchCatalogData: PropTypes.func
};


