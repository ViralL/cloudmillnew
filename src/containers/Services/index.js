import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

import { fetchCatalogData, setCurrentItem } from 'actions/CatalogActions';

import { Services } from './Services';

const mapStateToProps = (state) => ({
	catalog: state.catalog,
	point: state.point
});

const mapDispatchToProps = dispatch => ({
	setCurrentItem: (params) => setCurrentItem(dispatch, params),
	fetchCatalogData: (params) => fetchCatalogData(dispatch, params)
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Services));
