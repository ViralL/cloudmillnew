import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { bindActionCreators } from 'redux';

import { fetchAwardsData } from 'actions/AwardActions';

import { Approach } from './Approach';

const mapStateToProps = (state) => ({
	point: state.point,
	awards: state.awards
});

const mapDispatchToProps = dispatch => ({
	fetchAwardsData: bindActionCreators(fetchAwardsData, dispatch)
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Approach));
