import React, {Component, Fragment} from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';
import {DELAY, DURATION, POINT, URLS, DELAY_ROUTING} from 'Constants';
import {Parallax} from 'react-scroll-parallax';
import Fade from 'react-reveal/Fade';

// components
import Row from 'components/grid/Row';
import Col from 'components/grid/Col';
import Typography from 'components/markup/Typography';
import LinkBlock from 'components/partials/LinkBlock';
import Image from 'components/forms/Image';

import {scrollTo} from 'utils/scrollTo';

export class Approach extends Component {

	componentDidMount() {
		this.props.fetchAwardsData();
	}

	render() {
		const {point} = this.props.point;
		return (
			<Fragment>
				<div className={'breadcrumbs'}><Fade right duration={DURATION} delay={point === POINT.routing ? DELAY_ROUTING : 0}>Наш подход</Fade></div>
				<section className="indexPage approach">
					<div className={'container'}>
						<div className={'approachTarget'}>
							<Parallax y={[10, -10]}>
								<Fade bottom duration={DURATION} delay={point === POINT.routing ? DELAY_ROUTING : 0}>
									<Typography tag={'h4'}>
                                        выстроить для клиента<br/>
                                        эффективную коммуникацию<br/>
                                        с&nbsp;его аудиторией<br/>
                                        на&nbsp;базе ярких digital-продуктов
									</Typography>
								</Fade>
							</Parallax>
							<Fade bottom duration={DURATION} delay={point === POINT.routing ? DELAY_ROUTING : 0}>
								<div className={'approachTarget-img'}>
									<Parallax y={[-10, 10]}><Image src="/assets/images/approach/word_1.svg"
										alt=""/></Parallax>
									<Parallax className={'approachTarget-icon1'} y={[-25, 25]}><img
										src="/assets/images/approach/Target.svg" alt=""/></Parallax>
									<Parallax className={'approachTarget-icon2'} y={[-12, 12]}><img
										src="/assets/images/approach/Target_1.svg" alt=""/></Parallax>
								</div>
							</Fade>
						</div>
						<div className={'approachCharges'}>
							<Parallax y={[10, -10]}>
								<Fade bottom duration={DURATION} delay={point === POINT.routing ? DELAY_ROUTING : 0}>
									<Typography tag={'h4'}>
                                        ощущение пользы <br/>
                                        от&nbsp;разработанных проектов<br/>
                                        в&nbsp;виде измеримых результатов
									</Typography>
								</Fade>
							</Parallax>
							<Fade bottom duration={DURATION} delay={point === POINT.routing ? DELAY_ROUTING : 0}>
								<div className={'approachCharges-img'}>
									<Parallax y={[-10, 10]}><img src="/assets/images/approach/word_2.svg"
										alt=""/></Parallax>
									<Parallax className={'approachCharges-icon1'} y={[-15, 15]}><img
										src="/assets/images/approach/light_1.svg" alt=""/></Parallax>
									<Parallax className={'approachCharges-icon2'} y={[-14, 14]}><img
										src="/assets/images/approach/light_2.svg" alt=""/></Parallax>
									<Parallax className={'approachCharges-icon3'} y={[-13, 13]}><img
										src="/assets/images/approach/light_3.svg" alt=""/></Parallax>
									<Parallax className={'approachCharges-icon4'} y={[-12, 12]}><img
										src="/assets/images/approach/light_4.svg" alt=""/></Parallax>
								</div>
							</Fade>
						</div>
						<div className={'approachList'}>
							<Fade bottom duration={DURATION}>
								<Typography tag={'h2'} text={'Нас ценят, потому что:'}/>
							</Fade>
							<Row className={'between-xs'}>
								<Col xl={5} md={6} xs={12}>
									<Fade bottom duration={DURATION}>
										<Parallax y={[10, -10]} className="approachList-item">
                                        Любой проект у нас начинается
                                        с&nbsp;аналитики. Погружаемся
                                        в&nbsp;проекты, агрегируем данные. Как результат
                                        - <span>мы&nbsp;не&nbsp;угадываем</span>, а <span>опираемся на цифры</span>.
										</Parallax>
									</Fade>
								</Col>
								<Col xl={5} md={6} xs={12}>
									<Fade bottom delay={DELAY} duration={DURATION}>
										<Parallax y={[10, -10]} className="approachList-item">
                                        Мы используем итеративный подход, что
                                        означает <span>постоянный анализ и&nbsp;гибкость к&nbsp;изменениям</span> в&nbsp;процессе
                                        разработки.
										</Parallax>
									</Fade>
								</Col>
								<Col xl={5} md={6} xs={12}>
									<Fade bottom duration={DURATION}>
										<Parallax y={[10, -10]} className="approachList-item">
                                        Мы пропагандируем JTBD.
                                        Не&nbsp;стремимся разработать
										«самый лучший сайт» на&nbsp;все времена, разрабатываем цифровые
                                        продукты, <span>без раздувания</span> бюджета, решаем поставленные
                                        задачи <span>сегодня</span>.
										</Parallax>
									</Fade>
								</Col>
								<Col xl={5} md={6} xs={12}>
									<Fade bottom delay={DELAY} duration={DURATION}>
										<Parallax y={[10, -10]} className="approachList-item">
                                        Мы всегда помогаем маркетологу клиента утвердить проектные решения внутри
                                        компании.
                                        Наша <span>позиция всегда <br/> аргументирована</span>, <br/>
                                        а&nbsp;<span>доводы убедительны</span>.
										</Parallax>
									</Fade>
								</Col>
							</Row>
						</div>
						<Parallax className={'approachBack'} y={[-40, 40]}>
							<img src="/assets/images/approach/power.jpg" alt=""/>
						</Parallax>
						<Fade bottom duration={DURATION}>
							<Parallax className={'approachPower'} y={[-20, 20]}>
								<Row className={'between-xs'}>
									<Col md={6}>
										<div className={'approachPower-img'}><img
											src="/assets/images/approach/word_3.svg" alt=""/></div>
									</Col>
									<Col md={3} lg={2}>
										<div className={'indexPage-faqLink-body'}><Link to={URLS.contacts}
											className={'indexPage-faqLink spinIt'}/>
										</div>
									</Col>
								</Row>
							</Parallax>
						</Fade>
						<Row className={'approachPowerList between-xs'}>
							<Parallax sm={6} xs={12} className={'approachPowerList-item col-sm-6 col-xs-12'}
								y={[-12, 12]}>
								<Fade bottom duration={DURATION}>
									<Typography tag={'h2'} text={'Продакшн-офис в регионе'}/>
									<Typography tag={'p'} className={'text-md'}>
                                    Мы не&nbsp;отдаем разработку третьей стороне. Мы все делаем сами в&nbsp;своем
                                    региональном офисе в&nbsp;Воронеже. Контролируем процесс, делаем быстрее
                                    и&nbsp;дешевле.
									</Typography>
								</Fade>
							</Parallax>
							<Parallax sm={6} xs={12} className={'approachPowerList-item col-sm-6 col-xs-12'}
								y={[-15, 15]}>
								<Fade bottom duration={DURATION}>
									<Typography tag={'h2'}>
                                    Опыт <br/> и&nbsp;экспертиза
									</Typography>
									<Typography tag={'p'} className={'text-md'}>
                                    Копим опыт с&nbsp;2009 года, выпустили уже более 200 уникальных проектов. Мы
                                    не&nbsp;«молодые и динамично развивающиеся» - мы давно повзрослели и&nbsp;рады
                                    делиться своим профильным опытом.
									</Typography>
								</Fade>
							</Parallax>
							<Parallax sm={6} xs={12} className={'approachPowerList-item col-sm-6 col-xs-12'}
								y={[-11, 11]}>
								<Fade bottom duration={DURATION}>
									<Typography tag={'h2'} text={'CMS 1С-Битрикс'}/>
									<Typography tag={'p'} className={'text-md'}>
                                    Мы золотой сертифицированный партнер, участник программы «мониторинг качества».
                                    Знаем все тонкости и&nbsp;применимость системы. Порядка 90% наших проектов
                                    запущены на&nbsp;базе Битрикса.
									</Typography>
								</Fade>
							</Parallax>
							<Parallax sm={6} xs={12} className={'approachPowerList-item col-sm-6 col-xs-12'}
								y={[-15, 15]}>
								<Fade bottom duration={DURATION}>
									<Typography tag={'h2'} text={'Процессы'}/>
									<Typography tag={'p'} className={'text-md'}>
                                    Процессы важны. Очень. Мы наладили процессы и улучшаем их&nbsp;внутри своей
                                    компании. Мы хорошо знакомы с&nbsp;процессами внутри компании наших клиентов. Мы
                                    контролируем ситуацию и&nbsp;пониманием происходящее.
									</Typography>
								</Fade>
							</Parallax>
						</Row>
					</div>
				</section>
				<LinkBlock link={URLS.case} text={'Проекты'} class={'indexPage--backProj'}
					onClick={() => scrollTo(document.body, 0, 1250)}/>
			</Fragment>
		);
	}
}

Approach.propTypes = {
	awards: PropTypes.object,
	catalog: PropTypes.object,
	point: PropTypes.object,
	location: PropTypes.object,
	openPopup: PropTypes.func,
	fetchCatalogData: PropTypes.func,
	fetchAwardsData: PropTypes.func
};

