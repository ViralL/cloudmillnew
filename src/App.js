import React, {Component} from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import {withRouter} from 'react-router-dom';
import {ParallaxProvider} from 'react-scroll-parallax';
import Fade from 'react-reveal/Fade';

// redux
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

// actions
import {countLoading} from 'actions/LoadingActions';
import {setPoint} from 'actions/PointActions';
import {fetchCatalogData} from 'actions/CatalogActions';

// Constants
import {DURATION, POINT} from './Constants';

// components
import Header from 'components/partials/Header';
import Loading from 'components/animate/Loading';
import Linker from 'components/partials/Linker';
import Cursor from 'components/partials/Cursor';
import Popup from 'components/popups/index';

// utils
import {getElementOffset} from 'utils/getElement';
import {Magnetize} from 'utils/Magnet';

// styles
import 'assets/styles/app.scss';

class App extends Component {
	constructor(props) {
		super(props);
		const {isDark, loading} = this.props;
		this.state = {
			loading: loading.count === 0,
			color: !isDark,
			colorBottom: !isDark,
			mouseX: 0,
			mouseY: 0,
			touchClass: '',
			clientWidth: document.body.clientWidth,
			clicked: false,
			setBlackColor: false,
			isWhiteCursor: false,
			isHover: false,
		};
	}

	componentDidMount() {
		if (this.state.loading) {
			this.props.fetchCatalogData();
		}
		Magnetize();
		this.isLoad();
		setTimeout(() => window.scrollTo(0, 0), 1500);
		window.addEventListener('scroll', this.handleScroll.bind(this));
		window.addEventListener('resize', this.setScreenOrientation.bind(this));
		window.addEventListener('orientationchange', this.setScreenOrientation.bind(this));
	}

	componentDidUpdate(prevProps) {
		if (prevProps.location.pathname !== this.props.location.pathname) {
			this.props.setPoint(POINT.routing);
		}
	}

	componentWillUnmount() {
		const {isDark} = this.props;
		this.setState({
			color: !isDark
		});
		window.removeEventListener('scroll', this.handleScroll.bind(this));
	}

    setScreenOrientation = () => {
    	setTimeout(() => {
    		this.setState({clientWidth: document.body.clientWidth});
    	}, 100);
    }

    onMouseMove(event) {
    	const isLinkTag = event.target.tagName.toLowerCase() === 'a';
    	const isLinkClass = event.target.classList.contains('hoverLink');
    	this.setState({
    		mouseX: event.clientX,
    		mouseY: event.clientY + 60,
    		isHover: isLinkTag || isLinkClass,
    		isWhiteCursor: isLinkClass
    	});
    }

    onMouseDown() {
    	this.setState({touchClass: 'touchClass', clicked: true});
    	setTimeout(() => {
    		this.setState({clicked: false});
    	}, 100);
    }

    onMouseUp() {
    	this.setState({touchClass: ''});
    }

    handleScroll() {
    	const elemBlue = document.getElementsByClassName('indexPage--blue')[0] && getElementOffset(document.getElementsByClassName('indexPage--blue')[0]);
    	const elemWhite = document.getElementsByClassName('indexPage--white')[0] && getElementOffset(document.getElementsByClassName('indexPage--white')[0]);
    	const linker = document.getElementsByClassName('linker')[0] && document.getElementsByClassName('linker')[0].getBoundingClientRect().top;
    	const linkBlock = document.getElementsByClassName('frame')[0] && document.getElementsByClassName('frame')[0].getBoundingClientRect().top;

    	const elemBlueTop = elemBlue ? elemBlue.top : null;
    	const elemWhiteTop = elemWhite ? elemWhite.top : null;
    	const mouseColor = document.documentElement.scrollTop;
    	if (elemBlue) {
    		const isDarkBackground = (mouseColor + 450) > elemBlueTop;
    		this.setState({color: !isDarkBackground});
    	}
    	if (elemWhite) {
    		const isLightBackground = mouseColor >= elemWhiteTop - 80;
    		this.setState({color: isLightBackground, setBlackColor: isLightBackground});
    		if (elemBlue) {
    			const isDarkBackground = mouseColor > elemBlueTop;
    			this.setState({color: !isDarkBackground});
    		}
    	}
    	if (linker > linkBlock) {
    		this.setState({colorBottom: false});
    	} else {
    		const isDarkBackground = elemBlueTop !== null && mouseColor > elemBlueTop;
    		this.setState({colorBottom: !isDarkBackground});
    	}
    }


    isLoad() {
    	let count = 1;
    	this.props.countLoading(count++);
    	// temporary
    	setTimeout(() => {
    		this.setState({
    			loading: false
    		});
    	}, 3000);
    	// temporary
    }


    render() {
    	const {isDark, animationName, caseClassName, point, setBlackCursor} = this.props;
    	const {mouseX, mouseY, touchClass, clicked, color, isHover, colorBottom, setBlackColor, clientWidth} = this.state;
    	const containerClass = classNames('main', {'darkTheme': isDark, 'loaded': !this.state.loading}, animationName);
    	const blockZero = (isDark || (colorBottom ? !color : !setBlackColor)) ? '/assets/images/sign_0w.svg' : '/assets/images/sign_0b.svg';
    	const renderComponent = (
    		this.state.loading
    			? <Loading/>
    			: (
    				<main
    					className={`${containerClass} ${caseClassName}`}
    					onMouseMove={this.onMouseMove.bind(this)}
    					onMouseDown={this.onMouseDown.bind(this)}
    					onMouseUp={this.onMouseUp.bind(this)}
    				>
    					<Popup/>
    					<Cursor
    						mouseX={mouseX}
    						mouseY={mouseY}
    						isHover={isHover}
    						className={touchClass}
    						clicked={clicked}
    						setBlackCursor={setBlackCursor}
    						// color={isWhiteCursor ? false : showMenu ? false : color}
    					/>
    					<Header
    						isDark={isDark}
    						color={color}
    						point={point.point}
    					/>
    					<section className={'pageContent'}>
    						{clientWidth > 767 && (
    							<Fade
    								right
    								delay={point.point === POINT.routing ? 1000 : 0}
    								duration={DURATION}
    							>
    								<div className={'zeroBlock'}><img className="magnetize" src={blockZero} alt=""/></div>
    							</Fade>
    						)}
    						{clientWidth > 767 &&
                            <Linker point={point.point} setDark={!(isDark || (colorBottom ? !color : !setBlackColor))}/>}
    						{this.props.children}
    					</section>
    				</main>
    			)
    	);
    	return (
    		<ParallaxProvider>{renderComponent}</ParallaxProvider>
    	);
    }
}

App.propTypes = {
	animationName: PropTypes.string,
	caseClassName: PropTypes.string,
	point: PropTypes.object,
	loading: PropTypes.object,
	location: PropTypes.object,
	children: PropTypes.any,
	isDark: PropTypes.bool,
	setBlackCursor: PropTypes.bool,
	countLoading: PropTypes.func,
	fetchCatalogData: PropTypes.func,
	setPoint: PropTypes.func,
};

const mapStateToProps = (state) => ({
	loading: state.loading,
	point: state.point,
});

const mapDispatchToProps = dispatch => ({
	countLoading: bindActionCreators(countLoading, dispatch),
	fetchCatalogData: (params) => fetchCatalogData(dispatch, params),
	setPoint: bindActionCreators(setPoint, dispatch)
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(App));
