const rotation = [
	{
		img: '/assets/images/rotation/00.jpg'
	},
	{
		img: '/assets/images/rotation/01.jpg'
	},
	{
		img: '/assets/images/rotation/02.jpg'
	},
	{
		img: '/assets/images/rotation/03.jpg'
	},
	{
		img: '/assets/images/rotation/04.jpg'
	},
	{
		img: '/assets/images/rotation/05.jpg'
	},
	{
		img: '/assets/images/rotation/06.jpg'
	},
	{
		img: '/assets/images/rotation/07.jpg'
	},
	{
		img: '/assets/images/rotation/08.jpg'
	},
	{
		img: '/assets/images/rotation/09.jpg'
	},
	{
		img: '/assets/images/rotation/10.jpg'
	},
	{
		img: '/assets/images/rotation/11.jpg'
	},
	{
		img: '/assets/images/rotation/12.jpg'
	},
	{
		img: '/assets/images/rotation/13.jpg'
	},
	{
		img: '/assets/images/rotation/14.jpg'
	},
	{
		img: '/assets/images/rotation/15.jpg'
	}
];

export default rotation;
