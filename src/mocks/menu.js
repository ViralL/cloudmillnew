import {URLS} from 'Constants';

export const menu = [
	{
		text: 'О нас',
		link: URLS.about
	},
	{
		text: 'Наш подход',
		link: URLS.approach
	},
	{
		text: 'Услуги',
		link: URLS.services
	},
	{
		text: 'Проекты',
		link: URLS.case
	},
	{
		text: 'Контакты',
		link: URLS.contacts
	}
];

export const menu404 = [
	{
		text: 'Главная',
		link: URLS.index
	},
	{
		text: 'О нас',
		link: URLS.about
	},
	{
		text: 'Наш подход',
		link: URLS.approach
	},
	{
		text: 'Услуги',
		link: URLS.services
	},
	{
		text: 'Проекты',
		link: URLS.case
	},
	{
		text: 'Контакты',
		link: URLS.contacts
	}
];
