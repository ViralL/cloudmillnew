// import GLSL from 'glsl-transpiler';
import glsl from 'glslify';
import GyroNorm from 'utils/gyronorm';

/* eslint-disable */
const fragment = glsl(`#ifdef GL_ES
  precision mediump float;
#endif
uniform vec4 resolution;
uniform vec2 mouse;
uniform vec2 threshold;
uniform float time;
uniform float pixelRatio;
uniform sampler2D image0;
uniform sampler2D image1;
vec2 mirrored(vec2 v) {vec2 m = mod(v,2.);return mix(m,2.0 - m, step(1.0 ,m));}
void main() {
  vec2 uv = pixelRatio*gl_FragCoord.xy / resolution.xy ;
  vec2 vUv = (uv - vec2(0.5))*resolution.zw + vec2(0.5);
  vUv.y = 1. - vUv.y;
  vec4 tex1 = texture2D(image1,mirrored(vUv));
  vec2 fake3d = vec2(vUv.x + (tex1.r - 0.5)*mouse.x/threshold.x, vUv.y + (tex1.r - 0.5)*mouse.y/threshold.y);
  gl_FragColor = texture2D(image0,mirrored(fake3d));}`);
const vertex = glsl(`attribute vec2 a_position;void main() {gl_Position = vec4( a_position, 0, 1 );}`);
const gn = new GyroNorm.GyroNorm();

export const Sketch = () => {
  if (document.getElementById('gl')) {
    const container = document.getElementById('gl');
    const canvas = document.createElement('canvas');
    container.appendChild(canvas);
    const gl = canvas.getContext('webgl');
    const ratio = window.devicePixelRatio;
    const windowWidth = window.innerWidth;
    const windowHeight = window.innerHeight;
    let mouseX = 0;
		let mouseY = 0;

		let mouseTargetX = 0;
		let mouseTargetY = 0;

    const imageOriginal = container.getAttribute('data-imageoriginal');
    const imageDepth = container.getAttribute('data-imagedepth');
    const vth = container.getAttribute('data-verticalthreshold');
    const hth = container.getAttribute('data-horizontalthreshold');

    const imageURLs = [
      imageOriginal,
      imageDepth
    ];
    const textures = [];
		let program;
		let texture;
		let imageAspect;

		let uResolution;
		let uMouse;
		let uTime;
		let uRatio;
		let uThreshold;
		let billboard;

    // colors
    // #575757
    // #787878
    // #a7a7a7
    // #b7b7b7
    // #d6d6d6
    // #f7f7f7

    const startTime = new Date().getTime(); // Get start time for animating
    createScene();
    addTexture();
    mouseMove();
    gyro();

	function addShader( source, type ) {
	  const shader = gl.createShader( type );
	  gl.shaderSource( shader, source );
    gl.compileShader( shader );
	  const isCompiled = gl.getShaderParameter( shader, gl.COMPILE_STATUS );
	  if ( !isCompiled ) {
	    throw new Error( 'Shader compile error: ' + gl.getShaderInfoLog( shader ) );
	  }
	  gl.attachShader( program, shader );
	}

	function resizeHandler() {
  	const windowWidth = window.innerWidth;
  	const windowHeight = window.innerHeight;
		const width = container.offsetWidth;
		// this.height = this.width/this.aspect;
		const height = container.offsetHeight;

		canvas.width = width * ratio;
		canvas.height = height * ratio;
		canvas.style.width = width + 'px';
		canvas.style.height = height + 'px';
		let a1; let  a2;
		if(height / width < imageAspect) {
			a1 = 1;
			a2 = (height / width) / imageAspect;
		} else{
			a1 = (width / height) * imageAspect;
			a2 = 1;
		}
		uResolution.set( width, height, a1, a2 );
		uRatio.set( 1 / ratio );
		uThreshold.set( hth, vth );
		gl.viewport( 0, 0, width * ratio, height * ratio );
	}

	function resize() {
  	resizeHandler();
		window.addEventListener( 'resize', resizeHandler.bind(this) );
	}

	function createScene() {
    // create program
		program = gl.createProgram();
		// add shaders
		addShader( vertex, gl.VERTEX_SHADER );
		addShader( fragment, gl.FRAGMENT_SHADER );
		// link & use program
		gl.linkProgram( program );
		gl.useProgram( program );

		// create fragment uniforms
		uResolution = new Uniform( 'resolution', '4f', program, gl );
		uMouse = new Uniform( 'mouse', '2f', program, gl );
		uTime = new Uniform( 'time', '1f', program, gl );
		uRatio = new Uniform( 'pixelRatio', '1f', program, gl );
		uThreshold = new Uniform( 'threshold', '2f', program, gl );
		// create position attrib
		billboard = new Rect( gl );
		const positionLocation = gl.getAttribLocation( program, 'a_position' );
		gl.enableVertexAttribArray( positionLocation );
		gl.vertexAttribPointer( positionLocation, 2, gl.FLOAT, false, 0, 0 );
	}

	function addTexture() {
  	// const that = this;
		// gl = that.gl;
		loadImages(imageURLs, start.bind(this));
	}

	function start(images) {
		// const that = this;
		// gl = that.gl;

		// connect images
		imageAspect = images[0].naturalHeight / images[0].naturalWidth;
		for (let i = 0; i < images.length; i++) {


			texture = gl.createTexture();
			gl.bindTexture(gl.TEXTURE_2D, texture);

			// Set the parameters so we can render any size image.
			gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
			gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
			gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
			gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);

			// Upload the image into the texture.
			gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, images[i]);
			textures.push(texture);
		}

		// lookup the sampler locations.
		const u_image0Location = gl.getUniformLocation(program, 'image0');
		const u_image1Location = gl.getUniformLocation(program, 'image1');

		// set which texture units to render with.
		gl.uniform1i(u_image0Location, 0); // texture unit 0
		gl.uniform1i(u_image1Location, 1); // texture unit 1

		// Set each texture unit to use a particular texture.
		gl.activeTexture(gl.TEXTURE0);
		gl.bindTexture(gl.TEXTURE_2D, textures[0]);
		gl.activeTexture(gl.TEXTURE1);
		gl.bindTexture(gl.TEXTURE_2D, textures[1]);


		// start application
		resize();
		render();
	}


	function gyro() {
		const that = this;
		const maxTilt = 15;
		const rotationCoef = 0.15;

		gn.init({ gravityNormalized: true }).then(function() {
			gn.start(function(data) {

				const y = data.do.gamma;
				const x = data.do.beta;

				that.mouseTargetY = clamp(x, -that.maxTilt, that.maxTilt) / that.maxTilt;
				that.mouseTargetX = -clamp(y, -that.maxTilt, that.maxTilt) / that.maxTilt;

			});
		}).catch(function(e) {
			console.log('not supported');
			// Catch if the DeviceOrientation or DeviceMotion is not supported by the browser or device
		});

	}

	function mouseMove() {
  	// const that = this;
  	document.addEventListener('mousemove', function(e) {
			const halfX = windowWidth / 2;
			const halfY = windowHeight / 2;

  		mouseTargetX = (halfX - e.clientX) / halfX;
  		mouseTargetY = (halfY - e.clientY) / halfY;


  	});
	}


	function render() {
		const now = new Date().getTime();
		const currentTime = ( now - startTime ) / 1000;
		uTime.set( currentTime );
		// inertia
		mouseX += (mouseTargetX - mouseX) * 0.05;
		mouseY += (mouseTargetY - mouseY) * 0.05;


		uMouse.set( mouseX, mouseY );

		// render
		billboard.render( gl );
		requestAnimationFrame( render.bind(this) );
	}



  }
};

function loadImage(url, callback) {
	const image = new Image();
	image.src = url;
	image.onload = callback;
	return image;
}
function loadImages(urls, callback) {
	const images = [];
	let imagesToLoad = urls.length;

	// Called each time an image finished loading.
	const onImageLoad = function() {
		--imagesToLoad;
		// If all the images are loaded call the callback.
		if (imagesToLoad === 0) {
			callback(images);
		}
	};

	for (let ii = 0; ii < imagesToLoad; ++ii) {
		const image = loadImage(urls[ii], onImageLoad);
		images.push(image);
	}
}
function Uniform( name, suffix, program, gl ) {
	this.name = name;
	this.suffix = suffix;
	this.gl = gl;
	this.program = program;
	this.location = gl.getUniformLocation( program, name );
}

Uniform.prototype.set = function( ...values ) {
	const method = 'uniform' + this.suffix;
	const args = [ this.location ].concat( values );
	this.gl[ method ].apply( this.gl, args );
};

// ----- Rect ----- //
function Rect( gl ) {
	const buffer = gl.createBuffer();
	gl.bindBuffer( gl.ARRAY_BUFFER, buffer );
	gl.bufferData( gl.ARRAY_BUFFER, Rect.verts, gl.STATIC_DRAW );
}

Rect.verts = new Float32Array([
	-1, -1,
	1, -1,
	-1, 1,
	1, 1,
]);

Rect.prototype.render = function( gl ) {
	gl.drawArrays( gl.TRIANGLE_STRIP, 0, 4 );
};
function clamp(number, lower, upper) {
	if (number === number) {
		if (upper !== undefined) {
			number = number <= upper ? number : upper;
		}
		if (lower !== undefined) {
			number = number >= lower ? number : lower;
		}
	}
	return number;
}
