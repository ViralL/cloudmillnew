/* eslint-disable */
export const Magnetize = () => {
    $(document).on('mousemove touch', (e) => {
        $(window).width() > 768 && $('.magnetize').each(function() {
            !function (e, t) {
                var n, i, r, o = t.pageX, s = t.pageY, a = $(e), l = 20 * a.data('dist') || 120, c = a.offset().left + a.width() / 2, u = a.offset().top + a.height() / 2,
                    d = -.45 * Math.floor(c - o), f = -.45 * Math.floor(u - s);
                n = a, i = o, r = s, Math.floor(Math.sqrt(Math.pow(i - (n.offset().left + n.width() / 2), 2) + Math.pow(r - (n.offset().top + n.height() / 2), 2))) < l ? (TweenMax.to(a, .3, {
                    y: f,
                    x: d
                }), a.addClass('magnet')) : (TweenMax.to(a, .45, { y: 0, x: 0 }), a.removeClass('magnet'));
            }($(this), e);
        });
    })
}
