import axios from 'axios';

export function sendMessageOnServer(name, email, phone, description, file) {
	return new Promise((resolve, reject) => {
		const bodyFormData = new FormData();
		bodyFormData.set('name', name);
		bodyFormData.append('email', email);
		bodyFormData.append('phone', phone);
		description && bodyFormData.append('description', description);
		file && bodyFormData.append('file', file);
		axios({
			method: 'post',
			url: '/ajax/send.php',
			data: bodyFormData,
			config: {headers: {'Content-Type': 'multipart/form-data'}}
		})
			.then((response) => {
				resolve(response);
			})
			.catch((error) => {
				reject(error);
			});

	});
}
