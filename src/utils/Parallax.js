export function moveParallax(options) {
	options = options || {};
	const nameSpaces = {
		wrapper: options.wrapper || '.parallax',
		layers: options.layers || '.parallax-layer',
		deep: options.deep || 'data-parallax-deep'
	};
	const init = () => {
		const parallaxWrappers = document.querySelectorAll(nameSpaces.wrapper);
      	for(let i = 0; i < parallaxWrappers.length; i++) {
			(() => {
				parallaxWrappers[i].addEventListener('mousemove', (e) => {
					const x = e.clientX;
					const y = e.clientY;
					const layers = parallaxWrappers[i].querySelectorAll(nameSpaces.layers);
					for(let j = 0; j < layers.length; j++) {
						(() => {
							const deep = layers[j].getAttribute(nameSpaces.deep);
							const disallow = layers[j].getAttribute('data-parallax-disallow');
							const itemX = (disallow && disallow === 'x') ? 0 : x / deep;
							const itemY = (disallow && disallow === 'y') ? 0 : y / deep;
							if(disallow && disallow === 'both') return;
							layers[j].style.transform = 'translateX(' + itemX + '%) translateY(' + itemY + '%)';
						})(j);
					}
				});
			})(i);
      	}
	};
	init();
	return this;
}
