export const getElementOffset = (el) => {
	let top = 0;
	let left = 0;

	do {
		if (!isNaN(el.offsetTop)) {
			top += el.offsetTop;
		}
		if (!isNaN(el.offsetLeft)) {
			left += el.offsetLeft;
		}
		el = el.offsetParent;
	} while (el);

	return { top: top, left: left };
};
