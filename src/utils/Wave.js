import { displacement} from 'mocks/place';

/* eslint-disable */
export const wave = () => {
    var app = new PIXI.Application({
        width: 1200,
        height: 800,
        transparent: true
    });
    document.getElementsByClassName('canvas')[0].appendChild(app.view);

    var count = 0;

    // build a rope!
    var ropeLength = 918 / 20;

    var points = [];

    for (var i = 0; i < 20; i++) {
        points.push(new PIXI.Point(i * ropeLength, 0));
    }

    var strip = new PIXI.mesh.Rope(PIXI.Texture.fromImage('assets/images/text.png'), points);
    strip.x = -459;

    var snakeContainer = new PIXI.Container();
    snakeContainer.x = 400;
    snakeContainer.y = 300;

    snakeContainer.scale.set(800 / 1100);

    var sprite = PIXI.Sprite.fromImage(displacement);
    sprite.texture.baseTexture.wrapMode = PIXI.WRAP_MODES.REPEAT;

    var filter = new PIXI.filters.DisplacementFilter(sprite);

    app.stage.addChild(snakeContainer);

    snakeContainer.addChild(strip);

    var timeStamp = Math.floor(Date.now() / 1000);

    sprite.position.x = -300;//; - 300;
    sprite.position.y = 400;
    sprite.scale.y = 7;
    sprite.scale.x = 6;
    sprite.anchor.set(0.5);
    var timeStamp = Math.floor(Date.now() / 1000);

    app.ticker.add(function() {
        sprite.x -= 5;

        var timeStampNow = Math.floor(Date.now() / 1000);

        var tH = 388;
        var tHUP = 368;
        var tDOWN = 408;
        if((timeStampNow-timeStamp)>8) {
            timeStamp = timeStampNow;
            if(countW == 0){
                TweenMax.to(c[0], 0.5, {pixi:{alpha:0, y:tHUP}, ease:Power3.ease});
                c[1].y = tDOWN;
                TweenMax.to(c[1], 0.5, {pixi:{alpha:1, y:tH}, ease:Power3.ease, delay:0.5});
                
            }
            if(countW == 1){
                TweenMax.to(c[1], 0.5, {pixi:{alpha:0, y:tHUP}, ease:Power3.ease});
                c[2].y = tDOWN;
                TweenMax.to(c[2], 0.5, {pixi:{alpha:1, y:tH}, ease:Power3.ease, delay:0.5});
            }
            if(countW == 2){
                TweenMax.to(c[2], 0.5, {pixi:{alpha:0, y:tHUP}, ease:Power3.ease});
                c[0].y = tDOWN;
                TweenMax.to(c[0], 0.5, {pixi:{alpha:1, y:tH}, ease:Power3.ease, delay:0.5});
                countW = -1;
            }

            countW += 1;
        }
    });
};

export const posterWave = (mainWidth) => {
    var th = posterWave;
    var app = new PIXI.Application(mainWidth, 800, {transparent: true });
    const element = document.querySelector(".wrapper_render");
    element.appendChild(app.view);
    element.querySelector("canvas").style.width = '100%';
    element.querySelector("canvas").style.height = 500;

    th.elem = {
        $box: $("[data-poster-wave]"),
        $canvasBuffer: $('<canvas data-buffer class="opg"></canvas>'),
        $body: $("body")
    };

    th.data = {
        words: [],
        ctxBuffer: null
    };

    th.elem.$body.append(th.elem.$canvasBuffer);
    th.data.ctxBuffer = th.elem.$canvasBuffer[0].getContext('2d');
    th.elem.$canvasBuffer;

    th.buffer = {
        draw: function(fontFamily, fontSize, color, word) {
            var th = posterWave;

            th.data.ctxBuffer.font = fontSize+' '+fontFamily;
            th.data.ctxBuffer.fillStyle = color;
            th.data.ctxBuffer.textBaseline = 'top';

            var w = th.data.ctxBuffer.measureText(word+" ");
            th.elem.$canvasBuffer.attr({width:w.width*2}).attr({height:200*2}).css({width:w.width});
            th.data.ctxBuffer.restore();
            
            th.data.ctxBuffer.font = (Number(fontSize.slice(0, -2))*2)+'px '+fontFamily;
            th.data.ctxBuffer.fillStyle = color;
            th.data.ctxBuffer.textBaseline = 'top';
            th.data.ctxBuffer.fillText(word+" ", 0, 2);
            
            return { 
                image: th.elem.$canvasBuffer[0].toDataURL(),
                width: w.width
            };
        },
    };

    th.services = {
        getTextHeight:function(fontName, fontSize) {
            var th = posterWave;
            var text = $('<span>Hg</span>').css({'font-family': fontName, 'font-size' : fontSize}),
                block = $('<div style="display: inline-block; width: 1px; height: 0px;"></div>');
            
            var div = $('<div></div>');
                div.append(text, block);
            
                th.elem.$body.append(div);
            try {
                var result = {};
    
                block.css({ verticalAlign: 'top' });
                result.ascent = block.offset().top - text.offset().top;
    
                block.css({ verticalAlign: 'top' });
                result.height = block.offset().top - text.offset().top;
                result.descent = result.height - result.ascent;
            } finally {
                div.remove();
            }
            return result.height;					
        },
        getFontName:function(th){
            return th.css("font-family");
        },
        getFontSize:function(th){
            return th.css("font-size");
        },
        getFontColor:function(th){
            return th.css("color");
        }
    };
    
    const imageText = PIXI.Sprite.from('/assets/images/head_main-01.svg');
    imageText.anchor.set(0);
    imageText.x = 10;
    imageText.y = 10;
    imageText.width = app.screen.width / 1.3;
    imageText.height = app.screen.height * 0.72;
    app.stage.addChild(imageText);

    
    th.elem.$box.each(function (i, text) {
        th.data.words[i] = [];
        var d = th.data.words[i].d = {
            $this: $(this),
            tempText: [],
            tempWidth: [],
            tempWords: [],
            tempEff: [],
            sprite: [],
            filter: [],
            // fontname: 'CirceRegular',
            fontname: 'Arial',
            fontsize: '0',
            fontcolor: '#fff',
            height: th.services.getTextHeight(th.services.getFontName($(this)), th.services.getFontSize($(this))) + 10
        }
        d.$this.find("span").each(function (i, text) {
            d.tempText.push($(this).text());
        });

        d.$this.find("span").each(function (i, text) {
            d.tempText.push($(this).text());
        });


        d.$this.html("");

        var stateover = false;

        var _x = 0;
        var allWidth2 = 0;

        for (let w = 0; w != d.tempText.length / 2; w++) {
            var buffer = th.buffer.draw(d.fontname, d.fontsize, d.fontcolor, d.tempText[w]);
            allWidth2 += buffer.width * 2 + 162;
        }

        var allWidth = 0;

        for (let w = 0; w != d.tempText.length / 2; w++) {
            th.data.words[i].push(d.tempText[w]);
            var buffer = th.buffer.draw(d.fontname, d.fontsize, d.fontcolor, d.tempText[w]);
            d.tempEff[w] = PIXI.Sprite.fromImage(buffer.image);

            d.tempWidth[w] = buffer.width;
            allWidth += buffer.width;

            if (i == 0) {
                d.tempEff[w].x = _x;
                d.tempEff[w].startX = _x;
                _x += d.tempWidth[w] * 2 + 50;
            }

            d.tempEff[w].y = ((d.height + 158) * 1.2) * i;

            d.tempEff[w].interactive = true;
            if (w > 0 && i == 1) d.tempEff[w].alpha = 0.0;


            d.tempEff[w].w = w;

            app.stage.addChild(d.tempEff[w]);

        }
    });

    var countW = 0;
    var c = posterWave.data.words[1].d.tempEff;

    var sprite = PIXI.Sprite.fromImage(displacement);
    sprite.texture.baseTexture.wrapMode = PIXI.WRAP_MODES.REPEAT;

    var filter = new PIXI.filters.DisplacementFilter(sprite);
    app.stage.addChild(sprite);

    app.stage.filters = [filter];

    var count = 0;
    var stopper2 = $(window).width() * 2;
    sprite.position.x = $(window).width() * 2;//; - 300;
    sprite.position.y = 400;
    sprite.scale.y = 7;
    sprite.scale.x = 6;
    sprite.anchor.set(0.5);

    var timeStamp = Math.floor(Date.now() / 1000);
    app.ticker.add(function (delta) {
        sprite.x -= 5;

        var timeStampNow = Math.floor(Date.now() / 1000);

        var tH = 388;
        var tHUP = 368;
        var tDOWN = 408;
        // if ((timeStampNow - timeStamp) > 8) {
        //     timeStamp = timeStampNow;
        //     if (countW == 2) {
        //         TweenMax.to(c[2], 0.5, { pixi: { alpha: 0, y: tHUP }, ease: Power3.ease });
        //         c[0].y = tDOWN;
        //         TweenMax.to(c[0], 0.5, { pixi: { alpha: 1, y: tH }, ease: Power3.ease, delay: 0.5 });
        //         countW = -1;
        //     }

        //     countW += 1;
        // }
    });
};