
const tiltSettings = {
	movement: {
		imgWrapper: {
			translation: { x: 5, y: 5, z: 0 },
			reverseAnimation: { duration: 800, easing: 'easeOutQuart' }
		},
		caption: {
			translation: { x: 10, y: 10, z: [0, 50] },
			reverseAnimation: { duration: 1000, easing: 'easeOutQuart' }
		},
		shine: {
			translation: { x: 50, y: 50, z: 0 },
			reverseAnimation: { duration: 800, easing: 'easeOutQuart' }
		}
	}
};

export const tiltx = () => {
	[].slice.call(document.querySelectorAll('.tilter')).forEach((el) => {
		/* eslint-disable */
        new TiltFx(el, tiltSettings);
    });
}
