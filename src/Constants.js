const PATH = {
	base: process.env.NODE_ENV === 'production' ? '' : '' // temporary solution for path routing
};

const CATALOG_SIZE = 13;

const LOCAL_STORAGE_KEYS = {
	user: 'USER',
};

const ORIENTATION = {
	landscape: 'landscape',
	portrait: 'portrait',
};

const POINT = {
	routing: 'ROUTING',
	reload: 'RELOAD',
};

const TAB = {
	web: 'Сайты',
	brand: 'Брендинг',
	support: 'Поддержка',
};

const URLS = {
	index: PATH.base + '/',
	page404: PATH.base + '/*',
	googleBot: PATH.base + '/+&cd=1&hl=ru&ct=clnk&gl=lv',
	yandexBot: PATH.base + '/?_escaped_fragment_=',
	yandexBot2: PATH.base + '%2F&l10n=ru&mime=html&sign=ae55037634ea8c36f73e74964f5df952&keyno=0',
	yandexBot3: PATH.base + '/F&l10n=ru&mime=html&sign=ae55037634ea8c36f73e74964f5df952&keyno=0',
	yandexBot4: PATH.base + '/%2F&l10n=ru&mime=html&sign=ae55037634ea8c36f73e74964f5df952&keyno=0',
	test: PATH.base + '/test',
	about: PATH.base + '/about',
	aboutBot: PATH.base + '/about/*',
	case: PATH.base + '/projects',
	caseBot: PATH.base + '/projects/+&cd=1&hl=ru&ct=clnk&gl=lv',
	caseYBot: PATH.base + '%2F&l10n=ru&mime=html&sign=ae55037634ea8c36f73e74964f5df952&keyno=0',
	approach: PATH.base + '/approach',
	approachBot: PATH.base + '/approach/*',
	contacts: PATH.base + '/contacts',
	contactsBot: PATH.base + '/contacts/*',
	services: PATH.base + '/services',
	servicesBot: PATH.base + '/services/*'
};

const POPUPS = {
	resetPassword: 'ResetPasswordForm',
	changePassword: 'ChangePasswordForm',
	messagePopup: 'MessagePopup',
	authForm: 'AuthForm',
	registerForm: 'RegisterForm'
};

const DURATION = 500;
const DELAY = 200;
const DELAY_ROUTING = 1000;

const LINK_COUNTER = 3000;

const COMMON_ERROR = {
	emptyFields: 'Обязательные поля не заполнены',
	wrongCredentials: 'Неверные логин/пароль',
	technical: 'Техническая ошибка',
	failed: 'failed',
	wrongEmail: 'Неправильный email',
	wrongPhone: 'Неправильный номер телефона',
	emailWrong: 'Введите e-mail'
};


export {
	CATALOG_SIZE,
	LOCAL_STORAGE_KEYS,
	LINK_COUNTER,
	DELAY,
	TAB,
	DELAY_ROUTING,
	POINT,
	DURATION,
	ORIENTATION,
	COMMON_ERROR,
	URLS,
	POPUPS
};
