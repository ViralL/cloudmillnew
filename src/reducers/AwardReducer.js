import {AWARD_REQUEST, AWARD_SUCCESS, AWARD_FAILURE} from '../actions/types';

const initialState = {
	awards: [],
	isLoading: false,
	isError: false
};

export default (state = initialState, action) => {
	switch (action.type) {
		case AWARD_REQUEST:
			return {
				...state,
				...action.value
			};
		case AWARD_SUCCESS:
			return {
				...state,
				...action.value
			};
		case AWARD_FAILURE:
			return initialState;
		default:
			return state;
	}
};
