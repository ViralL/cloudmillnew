import {
	NAME_CHANGED,
	PHONE_CHANGED,
	DESCRIPTION_CHANGED,
	EMAIL_CHANGED,
	FILE_CHANGED,
	SEND_MESSAGE,
	SEND_MESSAGE_SUCCESS,
	SEND_MESSAGE_FAILURE
} from 'actions/types';

const initialState = {
	loading: false,
	description: '',
	email: '',
	name: '',
	file: '',
	phone: '',
	sent: false
};

export default (state = initialState, action) => {
	switch (action.type) {
		case NAME_CHANGED:
			return {
				...state,
				name: action.name
			};
		case PHONE_CHANGED:
			return {
				...state,
				phone: action.phone
			};
		case DESCRIPTION_CHANGED:
			return {
				...state,
				description: action.description
			};
		case EMAIL_CHANGED:
			return {
				...state,
				email: action.email
			};
		case FILE_CHANGED:
			return {
				...state,
				file: action.file
			};
		case SEND_MESSAGE:
			return {
				...state,
				loading: true,
				error: ''
			};
		case SEND_MESSAGE_SUCCESS:
			return {
				...state,
				loading: false,
				sent: true,
				error: ''
			};
		case SEND_MESSAGE_FAILURE:
			return {
				...state,
				sent: false,
				loading: false,
				error: action.error
			};
		default:
			return state;
	}
};
