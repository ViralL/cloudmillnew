import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';
import popup from './PopupReducer';
import catalog from './CatalogReducer';
import awards from './AwardReducer';
import contacts from './ContactsReducer';
import loading from './LoadingReducer';
import point from './PointReducer';
import tab from './TabReducer';

const rootReducer = (history) => combineReducers({
	catalog: catalog,
	awards: awards,
	popup: popup,
	tab: tab,
	point: point,
	loading: loading,
	contacts: contacts,
	router: connectRouter(history)
});

export default rootReducer;
