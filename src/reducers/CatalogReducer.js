import {CATALOG_REQUEST, CATALOG_SUCCESS, CATALOG_FAILURE, SET_CATALOG_ITEM} from '../actions/types';

const initialState = {
	catalog: {},
	current: null,
	isLoading: false,
	isError: false
};

export default (state = initialState, action) => {
	switch (action.type) {
		case CATALOG_REQUEST:
			return {
				...state,
				...action.value
			};
		case CATALOG_SUCCESS:
			return {
				...state,
				...action.value
			};
		case SET_CATALOG_ITEM:
			return {
				...state,
				current: action.index
			};
		case CATALOG_FAILURE:
			return initialState;
		default:
			return state;
	}
};
