import {SET_POINT} from '../actions/types';
import {POINT} from 'Constants';

const initialState = {
	point: POINT.reload
};

export default (state = initialState, action) => {
	switch (action.type) {
		case SET_POINT:
			return {
				point: action.point
			};
		default:
			return state;
	}
};
