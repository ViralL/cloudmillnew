import {NEWS_REQUEST, NEWS_SUCCESS, NEWS_FAILURE, SET_NEWS} from '../actions/types';

const initialState = {
	news: [],
	current: null,
	isLoading: false,
	isError: false
};

export default (state = initialState, action) => {
	switch (action.type) {
		case NEWS_REQUEST:
			return {
				...state,
				...action.value
			};
		case NEWS_SUCCESS:
			return {
				...state,
				...action.value
			};
		case SET_NEWS:
			return {
				...state,
				current: action.index
			};
		case NEWS_FAILURE:
			return initialState;
		default:
			return state;
	}
};
