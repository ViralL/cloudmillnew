import {LOADING, LOADING_IMAGE} from '../actions/types';

const initialState = {
	count: 0,
	imageLoader: false
};

export default (state = initialState, action) => {
	switch (action.type) {
		case LOADING:
			return {
				...state,
				count: action.index
			};
		case LOADING_IMAGE:
			return {
				...state,
				imageLoader: true
			};
		default:
			return state;
	}
};
