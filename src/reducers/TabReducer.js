import {SET_TAB} from '../actions/types';
import {TAB} from 'Constants';

const initialState = {
	tab: TAB.web
};

export default (state = initialState, action) => {
	switch (action.type) {
		case SET_TAB:
			return {
				tab: action.tab
			};
		default:
			return state;
	}
};
